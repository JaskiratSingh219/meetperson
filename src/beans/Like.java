package beans;

import java.sql.Date;

public class Like {
	String liker;
	String likee;
	Date date_time;
	
	public Like(String liker, String likee, Date date_time) {
		super();
		this.liker = liker;
		this.likee = likee;
		this.date_time = date_time;
	}

	public String getLiker() {
		return liker;
	}

	public void setLiker(String liker) {
		this.liker = liker;
	}

	public String getLikee() {
		return likee;
	}

	public void setLikee(String likee) {
		this.likee = likee;
	}

	public Date getDate_time() {
		return date_time;
	}

	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}
		
}
