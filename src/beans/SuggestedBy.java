package beans;

import java.util.Date;

public class SuggestedBy {
	String custRep;
	String profileB;
	String profileC;
	Date date_time;
	
	public SuggestedBy(String custRep, String profileB, String profileC, Date date_time) {
		super();
		this.custRep = custRep;
		this.profileB = profileB;
		this.profileC = profileC;
		this.date_time = date_time;
	}

	public String getCustRep() {
		return custRep;
	}

	public void setCustRep(String custRep) {
		this.custRep = custRep;
	}

	public String getProfileB() {
		return profileB;
	}

	public void setProfileB(String profileB) {
		this.profileB = profileB;
	}

	public String getProfileC() {
		return profileC;
	}

	public void setProfileC(String profileC) {
		this.profileC = profileC;
	}

	public Date getDate_time() {
		return date_time;
	}

	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}
	
}
