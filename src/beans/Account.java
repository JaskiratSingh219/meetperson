package beans;

import java.util.Date;

public class Account {
	String ownerSSN;
	double cardNumber;
	int accountNumber;
	Date accountCreationDate;
	
	public Account(String ownerSSN, double cardNumber, int accountNumber, Date accountCreationDate) {
		super();
		this.ownerSSN = ownerSSN;
		this.cardNumber = cardNumber;
		this.accountNumber = accountNumber;
		this.accountCreationDate = accountCreationDate;
	}

	public String getOwnerSSN() {
		return ownerSSN;
	}

	public void setOwnerSSN(String ownerSSN) {
		this.ownerSSN = ownerSSN;
	}

	public double getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(double cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getAccountCreationDate() {
		return accountCreationDate;
	}

	public void setAccountCreationDate(Date accountCreationDate) {
		this.accountCreationDate = accountCreationDate;
	}
	
}
