package beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User extends Person{
	String ppp;
	int rating;
	Date lastAccessed;
	
	public User(){
		super();
	}
	public User(String SSN, String password, String firstName, String lastName, String street, String city,
			String state, String zipcode, String email, String telephone, String ppp, int rating, Date lastAccessed) {
		super(SSN, password, firstName, lastName, street, city, state, zipcode, email, telephone);
		this.ppp = ppp;
		this.rating = rating;
		this.lastAccessed = lastAccessed;
	}
	
	public String getPpp() {
		return ppp;
	}
	public void setPpp(String ppp) {
		this.ppp = ppp;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Date getLastAccessed() {
		return lastAccessed;
	}
	public void setLastAccessed(Date lastAccessed) {
		this.lastAccessed = lastAccessed;
	}
	public void setLastAccessed(String lastAccessed) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
		try {
			this.lastAccessed = formatter.parse(lastAccessed);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
