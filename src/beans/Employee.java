package beans;

import java.util.Date;

public class Employee extends Person {
	String role;
	String startDate;
	int hourlyRate;
	
	public Employee(){
		super();
	}
	
	public Employee(String SSN, String password, String firstName, String lastName, String street, String city,
			String state, String zipcode, String email, String telephone, String role, String startDate, int hourlyRate) {
		super(SSN, password, firstName, lastName, street, city, state, zipcode, email, telephone);
		this.role = role;
		this.startDate = startDate;
		this.hourlyRate = hourlyRate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
//	public void setStartDate(String startDate){
//		this.startDate = SimpleDateFormat.parse(startDate);
//	}

	public int getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(int hourlyRate) {
		this.hourlyRate = hourlyRate;
	}
		
}
