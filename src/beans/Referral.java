package beans;

import java.util.Date;

public class Referral {
	String profileA;
	String profileB;
	String profileC;
	Date date_time;
	
	public Referral(String profileA, String profileB, String profileC, Date date_time) {
		super();
		this.profileA = profileA;
		this.profileB = profileB;
		this.profileC = profileC;
		this.date_time = date_time;
	}

	public String getProfileA() {
		return profileA;
	}

	public void setProfileA(String profileA) {
		this.profileA = profileA;
	}

	public String getProfileB() {
		return profileB;
	}

	public void setProfileB(String profileB) {
		this.profileB = profileB;
	}

	public String getProfileC() {
		return profileC;
	}

	public void setProfileC(String profileC) {
		this.profileC = profileC;
	}

	public Date getDate_time() {
		return date_time;
	}

	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}
	
}
