package beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Profile {
	String profileId;
	String ownerSSN;
	int age;
	int datingAgeRangeStart;
	int datingAgeRangeEnd;
	int datingGeoRange;
	String gender;
	ArrayList<String> hobbies;
	double height;
	int weight;
	String color;
	Date creationDate;
	Date lastModDate;
	
	public Profile(){
		super();
	}
	
	public Profile(String profileId, String ownerSSN, int age, int datingAgeRangeStart, int datingAgeRangeEnd,
			int datingGeoRange, String gender, ArrayList<String> hobbies, double height, int weight, String color,
			Date creationDate, Date lastModDate) {
		super();
		this.profileId = profileId;
		this.ownerSSN = ownerSSN;
		this.age = age;
		this.datingAgeRangeStart = datingAgeRangeStart;
		this.datingAgeRangeEnd = datingAgeRangeEnd;
		this.datingGeoRange = datingGeoRange;
		this.gender = gender;
		this.hobbies = hobbies;
		this.height = height;
		this.weight = weight;
		this.color = color;
		this.creationDate = creationDate;
		this.lastModDate = lastModDate;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getOwnerSSN() {
		return ownerSSN;
	}
	public void setOwnerSSN(String ownerSSN) {
		this.ownerSSN = ownerSSN;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getDatingAgeRangeStart() {
		return datingAgeRangeStart;
	}
	public void setDatingAgeRangeStart(int datingAgeRangeStart) {
		this.datingAgeRangeStart = datingAgeRangeStart;
	}
	public int getDatingAgeRangeEnd() {
		return datingAgeRangeEnd;
	}
	public void setDatingAgeRangeEnd(int datingAgeRangeEnd) {
		this.datingAgeRangeEnd = datingAgeRangeEnd;
	}
	public int getDatingGeoRange() {
		return datingGeoRange;
	}
	public void setDatingGeoRange(int datingGeoRange) {
		this.datingGeoRange = datingGeoRange;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public ArrayList<String> getHobbies() {
		return hobbies;
	}
	public void setHobbies(ArrayList<String> hobbies) {
		this.hobbies = hobbies;
	}
	public void setHobbies(String hobby){
		System.out.println("FFf: " + hobby);
		String[] parts = hobby.split(",");
		for(int a=0; a<parts.length; a++){
			hobbies.add(parts[a]);
		}
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public void setCreationDate(String creationDate) throws ParseException{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.creationDate = format.parse(creationDate);
	}
	public Date getLastModDate() {
		return lastModDate;
	}
	public void setLastModDate(Date lastModDate) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.lastModDate = lastModDate;
	}
	public void setLastModDate(String creationDate) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat();
		this.lastModDate = format.parse(creationDate);
	}

}
