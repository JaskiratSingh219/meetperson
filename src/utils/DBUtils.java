package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import beans.Account;
import beans.DateLog;
import beans.Employee;
import beans.Favorite;
import beans.Like;
import beans.Person;
import beans.Product;
import beans.Profile;
import beans.User;
import beans.UserAccount;

public class DBUtils {

	public static List<User> queryUser(Connection conn) throws SQLException {
		String sql = "Select a.SSN, a.PPP, a.Rating, a.DateofLastAct from User a ";
		String sql2 = "Select a.FirstName, a.LastName, a.Street, a.City, a.State,"
				+ "a.Zipcode, a.Email, a.Telephone from Person a where a.SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<User> list1 = new ArrayList<User>();
		while (rs.next()) {
			String SSN = rs.getString("SSN");
			String PPP = rs.getString("PPP");
			int rating = rs.getInt("Rating");
			Date lastAcs = rs.getDate("DateOfLastAct");
			User user = new User();
			user.setSsn(SSN);
			user.setPpp(PPP);
			user.setRating(rating);
			user.setLastAccessed(lastAcs);

			PreparedStatement pstm1 = conn.prepareStatement(sql2);
			pstm1.setString(1, SSN);

			ResultSet rs1 = pstm1.executeQuery();

			while (rs1.next()) {
				String firstName = rs1.getString("FirstName");
				String lastName = rs1.getString("LastName");
				String street = rs1.getString("Street");
				String city = rs1.getString("City");
				String state = rs1.getString("State");
				String zipcode = rs1.getString("Zipcode");
				String email = rs1.getString("Email");
				String telephone = rs1.getString("Telephone");

				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setStreet(street);
				user.setCity(city);
				user.setState(state);
				user.setZipcode(zipcode);
				user.setEmail(email);
				user.setTelephone(telephone);
			}

			list1.add(user);
		}
		return list1;
	}

	public static List<User> queryProfitableUsers(Connection conn) throws SQLException {
		String sql = "Select Profile1 as Profile, SUM(BookingFee) as TotalRevenue from"
				+ "(Select Profile1, BookingFee from Date Union " + "Select Profile2, BookingFee from Date) as T "
				+ "group by Profile1 " + "order by sum(BookingFee) DESC " + "limit 10";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<User> list1 = new ArrayList<User>();
		while (rs.next()) {
			String SSN = rs.getString("Profile");
			int rating = rs.getInt("TotalRevenue");
			User user = new User();
			user.setSsn(SSN);
			user.setRating(rating);

			list1.add(user);
		}
		return list1;
	}

	public static List<User> queryProfitableRep(Connection conn) throws SQLException {
		String sql = "Select CustRep, SUM(BookingFee) as TotalRevenue from Date "
				+ "group by CustRep order by sum(BookingFee) DESC limit 10";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<User> list1 = new ArrayList<User>();
		while (rs.next()) {
			String SSN = rs.getString("CustRep");
			int rating = rs.getInt("TotalRevenue");
			User user = new User();
			user.setSsn(SSN);
			user.setRating(rating);

			list1.add(user);
		}
		return list1;
	}

	public static List<User> queryMostActive(Connection conn) throws SQLException {
		String sql = "SELECT U.SSN, COUNT(*)TOTAL FROM USER U,Profile P,Date DP "
				+ "WHERE P.OwnerSSN=U.SSN AND ( DP.Profile1 = P.ProfileID) OR (DP.Profile2=P.ProfileID)"
				+ "GROUP BY U.SSN ORDER BY TOTAL DESC";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<User> list1 = new ArrayList<User>();
		while (rs.next()) {
			String SSN = rs.getString("SSN");
			int rating = rs.getInt("TOTAL");
			User user = new User();
			user.setSsn(SSN);
			user.setRating(rating);

			list1.add(user);
		}
		return list1;
	}
	
	public static List<DateLog> queryDate(Connection conn) throws SQLException {
		String sql = "Select * FROM date d";
		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<DateLog> list1 = new ArrayList<>();

		while (rs.next()) {
			String profile1 = rs.getString("Profile1");
			String profile2 = rs.getString("Profile2");
			String custRep = rs.getString("CustRep");
			Date date_time = rs.getDate("Date_Time");
			String location = rs.getString("Location");
			String comments = rs.getString("comments");
			int user1Rating = rs.getInt("User1Rating");
			int user2Rating = rs.getInt("User2Rating");
			Double bookingFee = rs.getDouble("BookingFee");
			
			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String s = fm.format(date_time);
			System.out.println(" THE STRING IS HAHAHAHAHAHAHHAHA THE STRING TIME IS BLAH "+s);
			DateLog date = new DateLog();
		
			date.setProfile1(profile1);
			date.setProfile2(profile2);
			date.setCustRep(custRep);
			date.setDate_time(s); 
			date.setLocation(location);
			date.setComments(comments);
			date.setUser1Rating(user1Rating);
			date.setUser2Rating(user2Rating);
			date.setLocation(location);
			date.setBookingFee(bookingFee);
		}
		return list1; 

	}

	public static List<Employee> queryEmployee(Connection conn) throws SQLException {
		String sql = "Select a.SSN, a.Role, a.StartDate, a.HourlyRate from Employee a";
		String sql2 = "Select a.FirstName, a.LastName, a.Street, a.City, a.State,"
				+ "a.Zipcode, a.Email, a.Telephone from Person a where a.SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<Employee> list1 = new ArrayList<>();
		while (rs.next()) {
			String SSN = rs.getString("SSN");
			String Role = rs.getString("Role");
			String startDate = rs.getString("StartDate");
			int hourlyRate = rs.getInt("HourlyRate");
			Employee user = new Employee();
			user.setSsn(SSN);
			user.setRole(Role);
			user.setStartDate(startDate);
			user.setHourlyRate(hourlyRate);

			PreparedStatement pstm1 = conn.prepareStatement(sql2);
			pstm1.setString(1, SSN);

			ResultSet rs1 = pstm1.executeQuery();

			while (rs1.next()) {
				String firstName = rs1.getString("FirstName");
				String lastName = rs1.getString("LastName");
				String street = rs1.getString("Street");
				String city = rs1.getString("City");
				String state = rs1.getString("State");
				String zipcode = rs1.getString("Zipcode");
				String email = rs1.getString("Email");
				String telephone = rs1.getString("Telephone");

				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setStreet(street);
				user.setCity(city);
				user.setState(state);
				user.setZipcode(zipcode);
				user.setEmail(email);
				user.setTelephone(telephone);
			}

			list1.add(user);
		}
		return list1;
	}

	public static Employee findEmployee(Connection conn, String ssn) throws SQLException {
		String sql = "Select a.SSN, a.Role, a.StartDate, a.HourlyRate from Employee a where a.SSN=?";
		String sql2 = "Select a.Password, a.FirstName, a.LastName, a.Street, a.City, a.State,"
				+ "a.Zipcode, a.Email, a.Telephone from Person a where a.SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);

		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {

			String SSN = rs.getString("SSN");
			String role = rs.getString("Role");
			Date startDate = rs.getDate("StartDate");
			int hourlyRate = rs.getInt("HourlyRate");

			Employee employee = new Employee();
			employee.setSsn(SSN);
			employee.setRole(role);
//			employee.setStartDate(startDate);
			employee.setHourlyRate(hourlyRate);

			PreparedStatement pstm1 = conn.prepareStatement(sql2);
			pstm1.setString(1, SSN);

			ResultSet rs1 = pstm1.executeQuery();

			while (rs1.next()) {
				String password = rs1.getString("Password");
				String firstName = rs1.getString("FirstName");
				String lastName = rs1.getString("LastName");
				String street = rs1.getString("Street");
				String city = rs1.getString("City");
				String state = rs1.getString("State");
				String zipcode = rs1.getString("Zipcode");
				String email = rs1.getString("Email");
				String telephone = rs1.getString("Telephone");

				employee.setPassword(password);
				employee.setFirstName(firstName);
				employee.setLastName(lastName);
				employee.setStreet(street);
				employee.setCity(city);
				employee.setState(state);
				employee.setZipcode(zipcode);
				employee.setEmail(email);
				employee.setTelephone(telephone);
			}

			return employee;
		}

		return null;
	}

	public static User findUser(Connection conn, String ssn) throws SQLException {
		String sql = "Select a.SSN, a.PPP, a.Rating, a.DateOfLastAct from User a where a.SSN=?";
		String sql2 = "Select a.Password, a.FirstName, a.LastName, a.Street, a.City, a.State,"
				+ "a.Zipcode, a.Email, a.Telephone from Person a where a.SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);

		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {

			String SSN = rs.getString("SSN");
			String ppp = rs.getString("PPP");
			int rating = rs.getInt("Rating");
			String lastAccessed = rs.getString("DateOfLastAct");

			User user = new User();
			user.setSsn(SSN);
			user.setPpp(ppp);
			user.setRating(rating);
			user.setLastAccessed(lastAccessed);

			PreparedStatement pstm1 = conn.prepareStatement(sql2);
			pstm1.setString(1, SSN);

			ResultSet rs1 = pstm1.executeQuery();

			while (rs1.next()) {
				String password = rs1.getString("Password");
				String firstName = rs1.getString("FirstName");
				String lastName = rs1.getString("LastName");
				String street = rs1.getString("Street");
				String city = rs1.getString("City");
				String state = rs1.getString("State");
				String zipcode = rs1.getString("Zipcode");
				String email = rs1.getString("Email");
				String telephone = rs1.getString("Telephone");

				user.setPassword(password);
				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setStreet(street);
				user.setCity(city);
				user.setState(state);
				user.setZipcode(zipcode);
				user.setEmail(email);
				user.setTelephone(telephone);
			}

			return user;
		}

		return null;
	}

	public static User findUserBySSN(Connection conn, String ssn) throws SQLException {
		String sql = "Select a.PPP, a.Rating, a.DateOfLastAct from User a where a.SSN=?";
		String sql2 = "Select a.Password, a.FirstName, a.LastName, a.Street, a.City, a.State,"
				+ "a.Zipcode, a.Email, a.Telephone from Person a where a.SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);

		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {

			String ppp = rs.getString("PPP");
			int rating = rs.getInt("Rating");
			// String lastAccessed = rs.getString("DateOfLastAct");

			User user = new User();
			user.setSsn(ssn);
			user.setPpp(ppp);
			user.setRating(rating);
			// user.setLastAccessed(lastAccessed);

			PreparedStatement pstm1 = conn.prepareStatement(sql2);
			pstm1.setString(1, ssn);

			ResultSet rs1 = pstm1.executeQuery();

			while (rs1.next()) {
				String firstName = rs1.getString("FirstName");
				String lastName = rs1.getString("LastName");
				String street = rs1.getString("Street");
				String city = rs1.getString("City");
				String state = rs1.getString("State");
				String zipcode = rs1.getString("Zipcode");
				String email = rs1.getString("Email");
				String telephone = rs1.getString("Telephone");

				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setStreet(street);
				user.setCity(city);
				user.setState(state);
				user.setZipcode(zipcode);
				user.setEmail(email);
				user.setTelephone(telephone);
			}

			return user;
		}

		return null;
	}

	public static Person findPerson(Connection conn, String username, String password) throws SQLException {
		String sql = "Select a.SSN, a.FirstName from Person a where a.Email=? and a.Password=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, username);
		pstm.setString(2, password);

		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {

			String SSN = rs.getString("SSN");
			String firstName = rs.getString("firstName");

			Person person = new Person();
			person.setSsn(SSN);
			person.setFirstName(firstName);

			System.out.println("Person" + person);

			return person;
		}

		return null;
	}

	public static void updateEmployee(Connection conn, Employee employee) throws SQLException {
		String sql = "Update Employee set Role=?, StartDate=?, HourlyRate=? where SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setString(1, employee.getRole());
//		pstm.setDate(2, (java.sql.Date) employee.getStartDate());
		pstm.setInt(3, employee.getHourlyRate());
		pstm.setString(4, employee.getSsn());
		pstm.executeUpdate();

		updatePerson(conn, employee);

	}

	public static void updatePerson(Connection conn, Person person) throws SQLException {
		String sql2 = "Update Person set Password=?, FirstName=?, LastName=?, Street=?,"
				+ "City=?, State=?, Zipcode=?, Email=?, Telephone=? where SSN=?";

		PreparedStatement pstm2 = conn.prepareStatement(sql2);

		pstm2.setString(1, person.getPassword());
		pstm2.setString(2, person.getFirstName());
		pstm2.setString(3, person.getLastName());
		pstm2.setString(4, person.getStreet());
		pstm2.setString(5, person.getCity());
		pstm2.setString(6, person.getState());
		pstm2.setString(7, person.getZipcode());
		pstm2.setString(8, person.getEmail());
		pstm2.setString(9, person.getTelephone());
		pstm2.setString(10, person.getSsn());
		pstm2.executeUpdate();
	}

	public static void insertEmployee(Connection conn, Employee employee) throws SQLException {
		String sql = "Insert into Employee(SSN, Role, StartDate, HourlyRate) values (?,?,?,?)";
		String sql2 = "Insert into Person(SSN, Password, FirstName, LastName, Street, City, State, Zipcode, Email, Telephone) values (?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setString(1, employee.getSsn());
		pstm.setString(2, employee.getRole());
		pstm.setString(3, employee.getStartDate());
		pstm.setInt(4, employee.getHourlyRate());

		PreparedStatement pstm2 = conn.prepareStatement(sql2);

		pstm2.setString(1, employee.getSsn());
		pstm2.setString(2, employee.getPassword());
		pstm2.setString(3, employee.getFirstName());
		pstm2.setString(4, employee.getLastName());
		pstm2.setString(5, employee.getStreet());
		pstm2.setString(6, employee.getCity());
		pstm2.setString(7, employee.getState());
		pstm2.setString(8, employee.getZipcode());
		pstm2.setString(9, employee.getEmail());
		pstm2.setString(10, employee.getTelephone());

		pstm2.executeUpdate();
		pstm.executeUpdate();
	}

	public static void deleteEmployee(Connection conn, String ssn) throws SQLException {
		String sql = "Delete From Employee where SSN=?";
		String sql2 = "Delete From Person where SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);
		pstm.executeUpdate();
		
		PreparedStatement pstm1 = conn.prepareStatement(sql2);
		pstm1.setString(1, ssn);
		pstm1.executeUpdate();
	}

	public static List<User> datedUsers(Connection conn, String ssn) throws SQLException {
		String sql = "Select OwnerSSN from Profile where ProfileID in "
				+ "(Select Profile2 as Profile From Date Where Profile1 in "
				+ "(Select ProfileID From Profile p Where p.OwnerSSN=?) " + "union "
				+ "Select Profile1 as Profile From Date Where Profile2 in "
				+ "(Select ProfileID From Profile p Where p.OwnerSSN=?))";

		String sql2 = "Select a.FirstName, a.LastName, a.Street, a.City, a.State,"
				+ "a.Zipcode, a.Email, a.Telephone from Person a where a.SSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);
		pstm.setString(2, ssn);

		ResultSet rs = pstm.executeQuery();
		List<User> list = new ArrayList<>();
		while (rs.next()) {
			String ownerSSN = rs.getString("OwnerSSN");

			PreparedStatement pstm1 = conn.prepareStatement(sql2);
			pstm1.setString(1, ownerSSN);

			ResultSet rs1 = pstm1.executeQuery();
			User user = new User();

			while (rs1.next()) {
				String firstName = rs1.getString("FirstName");
				String lastName = rs1.getString("LastName");
				String street = rs1.getString("Street");
				String city = rs1.getString("City");
				String state = rs1.getString("State");
				String zipcode = rs1.getString("Zipcode");
				String email = rs1.getString("Email");
				String telephone = rs1.getString("Telephone");

				user.setFirstName(firstName);
				user.setLastName(lastName);
				user.setStreet(street);
				user.setCity(city);
				user.setState(state);
				user.setZipcode(zipcode);
				user.setEmail(email);
				user.setTelephone(telephone);

				list.add(user);
			}
		}
		return list;
	}

	public static List<User> queryHighestRated(Connection conn) throws SQLException {
		String sql = "(Select b.SSN, b.Rating from User b Order By Rating DESC limit 3)";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<User> list = new ArrayList<User>();
		while (rs.next()) {
			String SSN = rs.getString("SSN");
			int rating = rs.getInt("Rating");
			User user = new User();
			user.setSsn(SSN);
			user.setRating(rating);
			list.add(user);
		}
		return list;
	}

	public static List<User> queryHighestRatedDateByCalendar(Connection conn) throws SQLException {

		String sql = "Select DATE(a.Date_Time) as Date, ((a.User1Rating + a.User2Rating)/2) as Average from Date a "
				+ "group by DATE(a.Date_Time), MONTH(a.Date_Time), YEAR(a.Date_Time) "
				+ "order by (User1Rating + User2Rating)/2 DESC " + "limit 5";

		PreparedStatement pstm = conn.prepareStatement(sql);

		ResultSet rs = pstm.executeQuery();
		List<User> list = new ArrayList<>();
		while (rs.next()) {
			String date = rs.getString("Date");
			int rating = rs.getInt("Average");
			User user = new User();
			user.setSsn(date);
			user.setRating(rating);
			list.add(user);
		}
		return list;

	}

	public static List<Profile> getProfilesBySSN(Connection conn, String ssn) throws SQLException, ParseException {
		String sql = "SELECT * FROM profile test " + "where test.OwnerSSN=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);

		ResultSet rs = pstm.executeQuery();
		List<Profile> list = new ArrayList<>();
		while (rs.next()) {
			String profileId = rs.getString("ProfileID");
			String ownerSSN = rs.getString("OwnerSSN");
			int age = rs.getInt("Age");
			int ageStart = rs.getInt("DatingAgeRangestart");
			int ageEnd = rs.getInt("DatingAgeRangeEnd");
			int geoRange = rs.getInt("DatinGeoRange");
			String gender = rs.getString("M_F");
			// String hobbies = rs.getString("Hobbies");
			double height = rs.getDouble("Height");
			int weight = rs.getInt("Weight");
			String hairColor = rs.getString("HairColor");
			String creationDate = rs.getString("CreationDate");
			String lastModDate = rs.getString("LastModDate");

			Profile profile = new Profile();

			profile.setProfileId(profileId);
			profile.setOwnerSSN(ownerSSN);
			profile.setAge(age);
			profile.setDatingAgeRangeStart(ageStart);
			profile.setDatingAgeRangeEnd(ageEnd);
			profile.setDatingGeoRange(geoRange);
			profile.setGender(gender);
			// profile.setHobbies(hobbies);
			profile.setHeight(height);
			profile.setWeight(weight);
			profile.setColor(hairColor);
			// profile.setCreationDate(creationDate);
			// profile.setLastModDate(lastModDate);

			list.add(profile);
		}
		return list;
	}

	public static List<String> recommendations(Connection conn, String ssn) throws SQLException {
		String sql = "Select Profile2 as Profile from Date where  User2Rating>3 and Profile1 in "
				+ "(Select ProfileID from Profile where OwnerSSN=?) " + "union "
				+ "Select Profile1 as Profile from Date where  User1Rating>3 and Profile2 in "
				+ "(Select ProfileID from Profile where OwnerSSN=?)";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);
		pstm.setString(2, ssn);

		ResultSet rs = pstm.executeQuery();
		List<String> list = new ArrayList<>();
		while (rs.next()) {
			String profileId = rs.getString("Profile");

			list.add(profileId);
		}
		return list;
	}

	public static List<Favorite> getFavorites(Connection conn, List<Profile> profiles) throws SQLException {
		String sql = "Select d.Likee, d.Date_Time from Likes d where d.Liker=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		List<Favorite> list = new ArrayList<>();

		for (Profile p : profiles) {
			pstm.setString(1, p.getProfileId());

			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				String likee = rs.getString("Likee");
				String date_time = rs.getString("Date_Time");
				String liker = p.getProfileId();

				Favorite fav = new Favorite(liker, likee, date_time);
				list.add(fav);
			}
		}
		return list;
	}

	public static List<DateLog> upcomingDates(Connection conn, List<Profile> profiles)
			throws SQLException, ParseException {
		String sql = "Select DATE(a.Date_Time) as Date, a.Profile1, a.Profile2, "
				+ "a.Comments, a.Date_Time, a.Location " + "From Date a " + "Where a.Profile1=? OR a.Profile2=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		List<DateLog> list = new ArrayList<>();

		for (Profile p : profiles) {
			pstm.setString(1, p.getProfileId());
			pstm.setString(2, p.getProfileId());

			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				String d = rs.getString("Date");

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date dat = df.parse(d);

				Calendar cal = Calendar.getInstance();

				if (dat.after(cal.getTime())) {
					String profile1 = rs.getString("Profile1");
					String profile2 = rs.getString("Profile2");
					String comments = rs.getString("Comments");
					String date_time = rs.getString("Date_Time");
					String location = rs.getString("Location");

					DateLog date = new DateLog();
					date.setProfile1(profile1);
					date.setProfile2(profile2);
					date.setComments(comments);
					date.setDate_time(date_time);
					date.setLocation(location);

					list.add(date);
				}
			}
		}
		return list;
	}

	public static List<DateLog> pastDates(Connection conn, List<Profile> profiles) throws SQLException, ParseException {
		String sql = "Select DATE(a.Date_Time) as Date, a.Profile1, a.Profile2, "
				+ "a.Comments, a.Date_Time, a.Location " + "From Date a " + "Where a.Profile1=? OR a.Profile2=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		List<DateLog> list = new ArrayList<>();

		for (Profile p : profiles) {
			pstm.setString(1, p.getProfileId());
			pstm.setString(2, p.getProfileId());

			ResultSet rs = pstm.executeQuery();

			while (rs.next()) {
				String d = rs.getString("Date");

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date dat = df.parse(d);

				Calendar cal = Calendar.getInstance();

				if (dat.before(cal.getTime())) {
					String profile1 = rs.getString("Profile1");
					String profile2 = rs.getString("Profile2");
					String comments = rs.getString("Comments");
					String date_time = rs.getString("Date_Time");
					String location = rs.getString("Location");

					DateLog date = new DateLog();
					date.setProfile1(profile1);
					date.setProfile2(profile2);
					date.setComments(comments);
					date.setDate_time(date_time);
					date.setLocation(location);

					list.add(date);
				}
			}
		}
		return list;
	}

	public static List<User> highestRatedProfiles(Connection conn) throws SQLException {
		String sql = "Select Profile, Rating from " + "(Select Profile1 as Profile, User2Rating as Rating from Date "
				+ "Union " + "Select Profile2 as Profile, User1Rating as Rating from Date) as T " + "group by profile "
				+ "order by rating desc " + "limit 5";

		PreparedStatement pstm = conn.prepareStatement(sql);
		List<User> list = new ArrayList<>();
		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {
			String ssn = rs.getString("Profile");
			int rating = rs.getInt("Rating");

			User user = new User();
			user.setSsn(ssn);
			user.setRating(rating);

			list.add(user);
		}
		return list;
	}

	public static List<User> mostActiveProfiles(Connection conn) throws SQLException {
		String sql = "Select Profile2 as Profile, sum(Total) as Total from "
				+ "(Select Profile2, count(Profile2) as Total from Date group by profile2 " + "union "
				+ "Select Profile1, count(Profile1) as Total from Date group by profile1) as T " + "group by Profile2 "
				+ "order by sum(Total) desc " + "limit 10";

		int a = 0;

		PreparedStatement pstm = conn.prepareStatement(sql);
		List<User> list = new ArrayList<>();
		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {
			String ssn = rs.getString("Profile");
			int rating = rs.getInt("Total");
			rating = ++a;

			User user = new User();
			user.setSsn(ssn);
			user.setRating(rating);

			list.add(user);
		}
		return list;
	}

	public static List<DateLog> mostPopularLocations(Connection conn) throws SQLException {
		String sql = "Select Location, count(Location) as Count From Date group by Location order by count(Location) DESC";

		PreparedStatement pstm = conn.prepareStatement(sql);
		List<DateLog> list = new ArrayList<>();
		ResultSet rs = pstm.executeQuery();
		int a = 0;

		while (rs.next()) {
			String location = rs.getString("Location");
			int r = rs.getInt("Count");
			r = (++a);

			DateLog date = new DateLog();
			date.setLocation(location);
			date.setUser1Rating(r);

			list.add(date);
		}
		return list;
	}

	public static List<Profile> browseProfiles(Connection conn, String ssn) throws SQLException, ParseException {
		String sql = "Select * from Profile a where a.OwnerSSN!=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);
		List<Profile> list = new ArrayList<>();
		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {
			String profileId = rs.getString("ProfileID");
			rs.getString("OwnerSSN");
			int age = rs.getInt("Age");
			int ageStart = rs.getInt("DatingAgeRangestart");
			int ageEnd = rs.getInt("DatingAgeRangeEnd");
			int geoRange = rs.getInt("DatinGeoRange");
			String gender = rs.getString("M_F");
			// String hobbies = rs.getString("Hobbies");
			double height = rs.getDouble("Height");
			int weight = rs.getInt("Weight");
			String hairColor = rs.getString("HairColor");
			String creationDate = rs.getString("CreationDate");
			String lastModDate = rs.getString("LastModDate");

			Profile profile = new Profile();

			profile.setProfileId(profileId);
			profile.setAge(age);
			profile.setDatingAgeRangeStart(ageStart);
			profile.setDatingAgeRangeEnd(ageEnd);
			profile.setDatingGeoRange(geoRange);
			profile.setGender(gender);
			// profile.setHobbies(hobbies);
			profile.setHeight(height);
			profile.setWeight(weight);
			profile.setColor(hairColor);
			// profile.setCreationDate(creationDate);
			// profile.setLastModDate(lastModDate);

			list.add(profile);
		}

		if (list.size() <= 0)
			System.out.println("Returning null!");
		return list;
	}

	public static Profile getProfile(Connection conn, String profileId) throws SQLException, ParseException {
		String sql = "Select * from Profile a where a.ProfileID=?";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, profileId);
		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {
			String profil = rs.getString("ProfileID");
			rs.getString("OwnerSSN");
			int age = rs.getInt("Age");
			int ageStart = rs.getInt("DatingAgeRangestart");
			int ageEnd = rs.getInt("DatingAgeRangeEnd");
			int geoRange = rs.getInt("DatinGeoRange");
			String gender = rs.getString("M_F");
			String hobbiesString = rs.getString("Hobbies");
			double height = rs.getDouble("Height");
			int weight = rs.getInt("Weight");
			String hairColor = rs.getString("HairColor");
			String creationDate = rs.getString("CreationDate");
			String lastModDate = rs.getString("LastModDate");

			ArrayList<String> hobbies = new ArrayList<>(Arrays.asList(hobbiesString.split(",")));

			Profile profile = new Profile();

			profile.setProfileId(profil);
			profile.setAge(age);
			profile.setDatingAgeRangeStart(ageStart);
			profile.setDatingAgeRangeEnd(ageEnd);
			profile.setDatingGeoRange(geoRange);
			profile.setGender(gender);
			profile.setHobbies(hobbies);
			profile.setHeight(height);
			profile.setWeight(weight);
			profile.setColor(hairColor);
			// profile.setCreationDate(creationDate);
			// profile.setLastModDate(lastModDate);

			return profile;
		}
		return null;
	}

	public static void addLike(Connection conn, Like like) throws SQLException {
		String sql = "Insert into Likes (Liker, Likee, Date_Time) values (?,?,?)";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, like.getLiker());
		pstm.setString(2, like.getLikee());
		pstm.setDate(3, like.getDate_time());

		pstm.executeUpdate();
	}

	public static void addDate(Connection conn, DateLog date) throws SQLException {
		String sql = "Insert into Date (Profile1, Profile2, Date_Time, Location, Comments) values (?,?,?,?,?)";

		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, date.getProfile1());
		pstm.setString(2, date.getProfile2());
		pstm.setString(3, date.getDate_time());
		pstm.setString(4, date.getLocation());
		// pstm.setLong(5, (long) date.getBookingFee());
		pstm.setString(5, date.getComments());

		pstm.executeUpdate();
	}

	public static void deleteDate(Connection conn, String p1, String p2, String dt) throws SQLException {
		String sql = "Delete From Date where Profile1=? and Profile2=? and Date_Time=?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setString(1, p1);
		pstm.setString(2, p2);
		pstm.setString(3, dt);

		pstm.executeUpdate();
	}

	public static void updateComments(Connection conn, String p1, String p2, String dt, String comments)
			throws SQLException {
		String sql = "Update Date set Comments=? where Profile1=? and Profile2=? and Date_Time=?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setString(1, comments);
		pstm.setString(2, p1);
		pstm.setString(3, p2);
		pstm.setString(4, dt);

		pstm.executeUpdate();
	}

	public static List<Profile> searchProfiles(Connection conn, int ageStart, int ageEnd, String gender, String hobbies,
			double height, String hairColor) throws SQLException, ParseException {
		String sql = "Select * from Profile where Age>? and Age<? and Height>? and Hobbies like ? and M_F like ? and HairColor like ?";

		PreparedStatement pstm = conn.prepareStatement(sql);

		pstm.setLong(1, ageStart);
		pstm.setLong(2, ageStart);
		pstm.setLong(3, (long) height);
		pstm.setString(4, hobbies);
		pstm.setString(5, gender);
		pstm.setString(6, hairColor);

		ResultSet rs = pstm.executeQuery();
		List<Profile> list = new ArrayList<>();

		while (rs.next()) {
			String profileId = rs.getString("ProfileID");
			rs.getString("OwnerSSN");
			int age = rs.getInt("Age");
			int aS = rs.getInt("DatingAgeRangestart");
			int aE = rs.getInt("DatingAgeRangeEnd");
			int geoRange = rs.getInt("DatinGeoRange");
			String m_f = rs.getString("M_F");
			String hobby = rs.getString("Hobbies");
			double heightt = rs.getDouble("Height");
			int weight = rs.getInt("Weight");
			String hC = rs.getString("HairColor");
			rs.getString("CreationDate");
			String lastModDate = rs.getString("LastModDate");

			Profile profile = new Profile();

			profile.setProfileId(profileId);
			profile.setAge(age);
			profile.setDatingAgeRangeStart(aS);
			profile.setDatingAgeRangeEnd(aE);
			profile.setDatingGeoRange(geoRange);
			profile.setGender(m_f);
			profile.setHobbies(hobbies);
			profile.setHeight(height);
			profile.setWeight(weight);
			profile.setColor(hairColor);
			// profile.setLastModDate(lastModDate);

			list.add(profile);
		}

		return list;
	}

	public static List<DateLog> bookingFeeForMonth(Connection conn, String month, String year) throws SQLException {

		String sql = "SELECT * FROM date d WHERE month(d.Date_Time)= ? and year(d.Date_Time)= ?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setInt(1, Integer.parseInt(month));
		pstm.setInt(2, Integer.parseInt(year));

		ResultSet rs = pstm.executeQuery();
		List<DateLog> list1 = new ArrayList<>();
		System.out.println("HAHHAHAHA LIST" + list1 + "Month= " + month + " Year =" + year);

		while (rs.next()) {
			String profile1 = rs.getString("Profile1");
			String profile2 = rs.getString("Profile2");
			String custRep = rs.getString("CustRep");
			Date date_time = rs.getDate("Date_Time");
			String location = rs.getString("Location");
			String comments = rs.getString("comments");
			int user1Rating = rs.getInt("User1Rating");
			int user2Rating = rs.getInt("User2Rating");
			Double bookingFee = rs.getDouble("BookingFee");

			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String s = fm.format(date_time);

			DateLog date = new DateLog();

			date.setProfile1(profile1);
			date.setProfile2(profile2);
			date.setCustRep(custRep);
			date.setDate_time(s);
			date.setLocation(location);
			date.setComments(comments);
			date.setUser1Rating(user1Rating);
			date.setUser2Rating(user2Rating);
			date.setLocation(location);
			date.setBookingFee(bookingFee);
			
			list1.add(date);
		}
		return list1;

	}

	public static List<Person> getCustomerByFirstLast(Connection conn, String firstName, String lastName)
			throws SQLException {

		String sql = "SELECT * FROM Person p, User u where p.FirstName=? and p.LastName=? and p.SSN=u.SSN";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, firstName);
		pstm.setString(2, lastName);
		ResultSet rs = pstm.executeQuery();
		List<Person> list1 = new ArrayList<>();
		System.out.println("TADAAAAAAA!!!  LIST" + list1 + " FirstName = " + firstName + " LAST NAME = " + lastName);
		while (rs.next()) {
			String ssn = rs.getString("SSN");
			String password = rs.getString("Password");
			String first = rs.getString("FirstName");
			String last = rs.getString("LastName");
			String street = rs.getString("Street");
			String city = rs.getString("City");
			String state = rs.getString("State");
			String zipcode = rs.getString("Zipcode");
			String email = rs.getString("Email");
			String telephone = rs.getString("Telephone");

			Person person = new Person(ssn, password, first, last, street, city, state, zipcode, email, telephone);
			list1.add(person);
		}
		return list1;

	}

	public static List<DateLog> dateByDate(Connection conn, String dateTime) throws SQLException {

		String dateTime2 = dateTime + " 23:59:59";
		dateTime = dateTime + " 00:00:00";
		String sql = "SELECT * FROM date d WHERE (d.Date_Time>=?) AND (d.Date_Time<=?)";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, dateTime);
		pstm.setString(2, dateTime2);

		ResultSet rs = pstm.executeQuery();
		List<DateLog> list1 = new ArrayList<>();
		System.out.println("HAHHAHAHA LIST" + list1 + "Date = " + dateTime);

		while (rs.next()) {
			String profile1 = rs.getString("Profile1");
			String profile2 = rs.getString("Profile2");
			String custRep = rs.getString("CustRep");
			Date date_time = rs.getDate("Date_Time");
			String location = rs.getString("Location");
			String comments = rs.getString("comments");
			int user1Rating = rs.getInt("User1Rating");
			int user2Rating = rs.getInt("User2Rating");
			Double bookingFee = rs.getDouble("BookingFee");
			
			System.out.println(date_time.toString()+" waaaaaaat");
			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
			String s = fm.format(date_time);

			DateLog date = new DateLog();

			date.setProfile1(profile1);
			date.setProfile2(profile2);
			date.setCustRep(custRep);
			date.setDate_time(s);
			date.setLocation(location);
			date.setComments(comments);
			date.setUser1Rating(user1Rating);
			date.setUser2Rating(user2Rating);
			date.setLocation(location);
			date.setBookingFee(bookingFee);
			
			list1.add(date);
		}
		return list1;

	}
	
	public static List<DateLog> getDateByCustomerSSN(Connection conn, String ssn)throws SQLException{
		
		
		System.out.println("getDateByCustomerSSN Querey in DBUtils where SSN = "+ssn);
		
		//String sql="SELECT * FROM date d WHERE d.Date_Time>=? 00:00:00 AND d.Date_Time<=? 23:59:59";
		String sql="Select * From date where profile1 in (Select ProfileID from Profile where OwnerSSN=?)"+ 
				"UNION Select * from Date where Profile2 in (Select ProfileID From Profile where OwnerSSN=?)";
		PreparedStatement pstm=conn.prepareStatement(sql);
		pstm.setString(1,ssn);
		pstm.setString(2, ssn);
		
		ResultSet rs=pstm.executeQuery();
		List<DateLog> list1 = new ArrayList<>();
		System.out.println("HAHHAHAHA LIST"+list1);
		
		while (rs.next()) {
			String profile1 = rs.getString("Profile1");
			String profile2 = rs.getString("Profile2");
			String custRep = rs.getString("CustRep");
			Date date_time = rs.getDate("Date_Time");
			String location = rs.getString("Location");
			String comments = rs.getString("comments");
			int user1Rating = rs.getInt("User1Rating");
			int user2Rating = rs.getInt("User2Rating");
			Double bookingFee = rs.getDouble("BookingFee");
			
			SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String s = fm.format(date_time);
			
			DateLog date = new DateLog();

			date.setProfile1(profile1);
			date.setProfile2(profile2);
			date.setCustRep(custRep);
			date.setDate_time(s);
			date.setLocation(location);
			date.setComments(comments);
			date.setUser1Rating(user1Rating);
			date.setUser2Rating(user2Rating);
			date.setLocation(location);
			date.setBookingFee(bookingFee);
			
			list1.add(date);
		} 
		return list1; 
				
	}
	
	public static List<Person> getCustomerList(Connection conn)throws SQLException {
		String sql="SELECT * FROM Person p, User u where p.SSN=u.SSN";
		PreparedStatement pstm=conn.prepareStatement(sql);
		
		ResultSet rs=pstm.executeQuery();
		List<Person> list1 = new ArrayList<>();
		while (rs.next()) {
			String ssn = rs.getString("SSN");
			String password = rs.getString("Password");
			String first = rs.getString("FirstName");
			String last = rs.getString("LastName");
			String street = rs.getString("Street");
			String city = rs.getString("City");
			String state = rs.getString("State");
			String zipcode = rs.getString("Zipcode");
			String email = rs.getString("Email");
			String telephone = rs.getString("Telephone"); 
			
			Person person= new Person(ssn,password,first,last,street,city,state,zipcode,email,telephone); 
			list1.add(person);
		} 
		return list1; 
	}
	
	public static void updateCustomerInformation(Connection conn, Person p) throws SQLException {
		String sql = "UPDATE Person set Password= ?, FirstName=?, LastName =?, Street=?,City=?,Zipcode=?,Email =?,Telephone=? WHERE SSN=?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, p.getPassword());
		pstm.setString(2, p.getFirstName());
		pstm.setString(3, p.getLastName());
		pstm.setString(4, p.getStreet());
		pstm.setString(5, p.getCity());
		pstm.setString(6, p.getZipcode());
		pstm.setString(7, p.getEmail());
		pstm.setString(8, p.getTelephone());
		pstm.setString(9, p.getSsn());

		pstm.executeUpdate();
	}
	public static void updateProfileForCustomer(Connection conn, Profile p)throws SQLException{
		String sql="UPDATE Profile set age=?, datingAgeRangeStart=?, datingAgeRangeEnd=?,datinGeoRange=?,M_F=?,hobbies=?,weight=?,height=?,HairColor=?,lastModDate=? where profileID=?";
		PreparedStatement preparedStatement=conn.prepareStatement(sql); 
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setInt(1, p.getAge());
		pstm.setInt(2, p.getDatingAgeRangeStart());
		pstm.setInt(3, p.getDatingAgeRangeEnd());
		pstm.setInt(4, p.getDatingGeoRange());
		pstm.setString(5, p.getGender());
		String idList =p.getHobbies().toString();
		System.out.println("--------------------------IDLIST"+idList);
		String csv = idList.substring(1, idList.length() - 1).replace(", ", ",");
		pstm.setString(6, csv);
		pstm.setInt(7, p.getWeight());
		pstm.setDouble(8, p.getHeight());
		pstm.setString(9, p.getColor());
		pstm.setDate(10,new java.sql.Date(p.getLastModDate().getTime()));
		pstm.setString(11, p.getProfileId());
		System.out.println("PROFILE ID = "+p.getProfileId());
		
	pstm.executeUpdate();
		
	
		
		
	}
	
	public static Person getSpecificCustomer(Connection conn, String social) throws SQLException {
		String sql = "SELECT * FROM Person p WHERE p.SSN=?";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, social);
		ResultSet rs = pstm.executeQuery();
		Person person = new Person();
		while (rs.next()) {
			String ssn = social;
			String password = rs.getString("Password");
			String firstName = rs.getString("FirstName");
			String lastName = rs.getString("LastName");
			String street = rs.getString("Street");
			String city = rs.getString("City");
			String state = rs.getString("State");
			String zipcode = rs.getString("Zipcode");
			String email = rs.getString("Email");
			String telephone = rs.getString("Telephone");

			person.setSsn(ssn);
			person.setPassword(password);
			person.setFirstName(firstName);
			person.setLastName(lastName);
			person.setStreet(street);
			person.setState(state);
			person.setCity(city);
			person.setZipcode(zipcode);
			person.setEmail(email);
			person.setTelephone(telephone);
			// Person person= new
			// Person(ssn,password,first,last,street,city,state,zipcode,email,telephone);
			return person;
		}

		return null;
	}
	
	public static List<Profile> getCustomerProfile(Connection conn, String ssn) throws SQLException {
		String sql = "SELECT * FROM Profile prof,Person p where p.SSN=? AND prof.OwnerSSN=? ";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, ssn);
		pstm.setString(2, ssn);
		ResultSet rs = pstm.executeQuery();

		List<Profile> list1 = new ArrayList<>();
		System.out.println("HAHHAHAHA Profile List" + list1);

		while (rs.next()) {
			String profileID = rs.getString("ProfileID");
			String ownerSSN = rs.getString("OwnerSSN");
			int age = rs.getInt("Age");
			int datingAgeRangeStart = rs.getInt("DatingAgeRangeStart");
			int datingAgeRangeEnd = rs.getInt("DatingAgeRangeEnd");
			int datingGeoRange = rs.getInt("DatinGeoRange");
			String gender = rs.getString("M_F");
			double height = rs.getDouble("Height");
			String hobbiesString = rs.getString("Hobbies");
			System.out.println("THIS PERSON HOOBIES ARE :" + hobbiesString);
			int weight = rs.getInt("Weight");
			String color = rs.getString("HairColor");
			Date creationDate = rs.getDate("CreationDate");
			Date lastModDate = rs.getDate("LastModDate");
			ArrayList<String> hobbies = new ArrayList<String>(Arrays.asList(hobbiesString.split(",")));
			Profile profile = new Profile(profileID, ownerSSN, age, datingAgeRangeStart, datingAgeRangeEnd,
					datingGeoRange, gender, hobbies, height, weight, color, creationDate, lastModDate);
			list1.add(profile);
		}
		return list1;

	}
	
	public static Profile getCustomerProfileByID(Connection conn, String profileId) throws SQLException {
		String sql = "SELECT * FROM Profile where ProfileID=? ";
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setString(1, profileId);
		ResultSet rs = pstm.executeQuery();

		while (rs.next()) {
			String profileID = rs.getString("ProfileID");
			String ownerSSN = rs.getString("OwnerSSN");
			int age = Integer.parseInt(rs.getString("Age"));
			int datingAgeRangeStart = rs.getInt("DatingAgeRangeStart");
			int datingAgeRangeEnd = rs.getInt("DatingAgeRangeEnd");
			int datingGeoRange = rs.getInt("DatinGeoRange");
			String gender = rs.getString("M_F");
			double height = rs.getDouble("Height");
			String hobbiesString = rs.getString("Hobbies").replace("[", "");
			hobbiesString.replace("]", "");
			System.out.println("THIS PERSON HOOBIES ARE :" + hobbiesString);
			int weight = rs.getInt("Weight");
			String color = rs.getString("HairColor");
			Date creationDate = rs.getDate("CreationDate");
			Date lastModDate = rs.getDate("LastModDate");
			System.out.println("HOBBIES STRING ----->"+hobbiesString);
			ArrayList<String> hobbies = new ArrayList<>(Arrays.asList(hobbiesString.split(",")));
			Profile profile = new Profile(profileID, ownerSSN, age, datingAgeRangeStart, datingAgeRangeEnd,
					datingGeoRange, gender, hobbies, height, weight, color, creationDate, lastModDate);
			return profile;
		}
		return null;

	}


	public static Account getCustomerAccount(Connection conn, String ssn) throws SQLException {
		String sql="SELECT * FROM ACCOUNT WHERE ownerSSN=?";
		PreparedStatement pstm=conn.prepareStatement(sql);
		pstm.setString(1, ssn);
		ResultSet rs=pstm.executeQuery();
		
		while(rs.next()){
			
			String ownerSSN=rs.getString("ownerSSN");
			double cardNumber=rs.getDouble("cardNumber");
			int accountNumber=rs.getInt("acctNum");
			Date accountCreationDate=rs.getDate("AcctCreationDate");
			Account account=new Account(ownerSSN,cardNumber,accountNumber,accountCreationDate);
			
			return account;
		}
		
		return null;
	}
	
	public static void updateCustomerAccount(Connection conn, double cardNumber, String ssn) throws SQLException{
		String sql="UPDATE ACCOUNT Set cardNumber=? Where ownerSSN=?";
		PreparedStatement pstm=conn.prepareStatement(sql);
		pstm.setDouble(1, cardNumber);
		pstm.setString(2, ssn);
		pstm.executeUpdate();
		
		
	}

}
