package filter;

import java.io.IOException;
import java.util.Date;
 
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Person;
import utils.MyUtils;

@WebFilter(filterName = "logFilter", servletNames = "EmployeeListServlet")
public class LogFilter implements Filter {
	private ServletContext context;
 
    public LogFilter() {
    }
 
    @Override
    public void init(FilterConfig fConfig) throws ServletException {
    	this.context = fConfig.getServletContext();
        this.context.log("AuthenticationFilter initialized");
    }
 
    @Override
    public void destroy() {
//        System.out.println("LogFilter destroy!");
    }
 
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
 
    	System.out.println("HIIII");
    	
    	HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        
        HttpSession session = ((HttpServletRequest) request).getSession();

        Person person = MyUtils.getLoginedPerson(session);

        if (person == null) {   //checking whether the session exists
        	System.out.println("NULLLLL");
            this.context.log("Unauthorized access request");
            res.sendRedirect(req.getContextPath() + "/login");
        } else{
        	String type = MyUtils.getLoginedType(session);
        	if(type.equals("Manager"))chain.doFilter(request, response);
        	else{
        		res.sendRedirect(req.getContextPath() + "/unauthorizedAccess");
        	}
            
        }
    }
 
}
