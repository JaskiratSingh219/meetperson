package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import beans.Employee;
import utils.DBUtils;
import utils.MyUtils;
 
@WebServlet(urlPatterns = { "/man/createEmployee" })
public class CreateEmployeeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public CreateEmployeeServlet() {
        super();
    }
 
    // Show product creation page.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/createEmployee.jsp");
        dispatcher.forward(request, response);
    }
 
    // When the user enters the product information, and click Submit.
    // This method will be called.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String ssn = (String) request.getParameter("ssn");
        String password = (String)request.getParameter("password");
        String firstName = (String) request.getParameter("firstName");
        String lastName = (String) request.getParameter("lastName");
        String street = (String) request.getParameter("street");
        String city = (String) request.getParameter("city");
        String state = (String) request.getParameter("state");
        String zipcode = (String) request.getParameter("zipcode");
        String email = (String) request.getParameter("email");
        String telephone = (String) request.getParameter("telephone");
        String role = (String) request.getParameter("role");
        String startDate = (String) request.getParameter("startDate");
        int hourlyRate = Integer.parseInt(request.getParameter("hourlyRate"));
        
        Employee employee = new Employee(ssn, password, firstName, lastName, street, city, state, zipcode, email, telephone, role, startDate, hourlyRate);
        
//        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
//        Date d = null;
//		try {
//			d = (Date) s.parse(startDate);
//		} catch (ParseException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//        
//        employee.setStartDate(d);
        
 
        String errorString = null;
 
//        // Product ID is the string literal [a-zA-Z_0-9]
//        // with at least 1 character
//        String regex = "\\w+";
// 
//        if (code == null || !code.matches(regex)) {
//            errorString = "Product Code invalid!";
//        }
 
        if (errorString == null) {
            try {
                DBUtils.insertEmployee(conn, employee);
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }
        }
 
        // Store infomation to request attribute, before forward to views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("product", employee);
 
        // If error, forward to Edit page.
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/createEmployee.jsp");
            dispatcher.forward(request, response);
        }
        // If everything nice.
        // Redirect to the product listing page.
        else {
            response.sendRedirect(request.getContextPath() + "/man/employeeList");
        }
    }
 
}