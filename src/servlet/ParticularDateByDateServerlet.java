package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DateLog;
import utils.DBUtils;
import utils.MyUtils;

/**
 * Servlet implementation class particularDateByDate
 */
@WebServlet("/particularDateByDate")
public class ParticularDateByDateServerlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn=MyUtils.getStoredConnection(request); 
        SimpleDateFormat format=new SimpleDateFormat("yyyy-mm-dd");
        Date dateFormat = null;
		try {
			dateFormat = format.parse(request.getParameter("date"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        String correctFormatDate=dateFormat+""; 
        System.out.println(correctFormatDate+" Date being processed");
        
        String errorString=null;
        List<DateLog> list = null;
        try{
        	list=DBUtils.dateByDate(conn, correctFormatDate);
        }catch (SQLException e) {
        	System.out.println("ERROR LINE particulardatebydate serverlet MONTH/YEAR TRY CATCH!!!!!!!!!!!!"+correctFormatDate);
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        request.setAttribute("errorString", errorString);
        //request.setAttribute("employeeList", list);
        request.setAttribute("salesReportForAParticularMonth",list );
        System.out.printf("list:", list.toString());
         
        // Forward to /WEB-INF/views/productListView.jsp
       
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/monthView.jsp");
        dispatcher.forward(request, response);
        //	response.sendRedirect(request.getContextPath() + "/SalesReportByMonth");	
    
	}

}
