package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
 
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import beans.Employee;
import utils.DBUtils;
import utils.MyUtils;
 
@WebServlet(urlPatterns = { "/man/editEmployee" })
public class EditEmployeeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    public EditEmployeeServlet() {
        super();
    }
 
    // Show product edit page.
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String ssn = (String) request.getParameter("ssn");
 
        Employee employee = null;
 
        String errorString = null;
 
        try {
            employee = DBUtils.findEmployee(conn, ssn);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
 
        // If no error.
        // The product does not exist to edit.
        // Redirect to productList page.
        if (errorString != null && employee == null) {
            response.sendRedirect(request.getServletPath() + "/employeeList");
            return;
        }
        

        // Store errorString in request attribute, before forward to views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("employee", employee);
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/editEmployeeView.jsp");
        dispatcher.forward(request, response);
 
    }
 
    // After the user modifies the product information, and click Submit.
    // This method will be executed.
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String ssn = (String) request.getParameter("ssn");
        String password = (String)request.getParameter("password");
        String firstName = (String) request.getParameter("firstName");
        String lastName = (String) request.getParameter("lastName");
        String street = (String) request.getParameter("street");
        String city = (String) request.getParameter("city");
        String state = (String) request.getParameter("state");
        String zipcode = (String) request.getParameter("zipcode");
        String email = (String) request.getParameter("email");
        String telephone = (String) request.getParameter("telephone");
        String role = (String) request.getParameter("role");
//        String startDate = (String) request.getParameter("startDate");
        int hourlyRate = Integer.parseInt(request.getParameter("hourlyRate"));
        
        Employee employee = new Employee(ssn, password, firstName, lastName, street, city, state, zipcode, email, telephone, role, null, hourlyRate);
 
        String errorString = null;
 
        try {
            DBUtils.updateEmployee(conn, employee);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        // Store infomation to request attribute, before forward to views.
        request.setAttribute("errorString", errorString);
        request.setAttribute("employee", employee);
 
        // If error, forward to Edit page.
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/editEmployeeView.jsp");
            dispatcher.forward(request, response);
        }
        // If everything nice.
        // Redirect to the product listing page.
        else {
            response.sendRedirect(request.getContextPath() + "/employeeList");
        }
    }
 
}