package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DateLog;

import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/man/SalesReportByMonth" })
public class SalesReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SalesReportServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String errorString = null;
		List<DateLog> list = null;
		try {
			list = DBUtils.queryDate(conn);
		} catch (SQLException e) {
			System.out.println("ERROR LINE 38 SALESREPORTSESERVELET TRY CATCH!!!!!!!!!!!!");
			e.printStackTrace();
			errorString = e.getMessage();
		}
		// Store info in request attribute, before forward to views
		request.setAttribute("errorString", errorString);
		// request.setAttribute("employeeList", list);
		request.setAttribute("salesReportByMonthList", list);
		System.out.printf("list:", list.toString());

		// Forward to /WEB-INF/views/productListView.jsp
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/salesReportByMonthView.jsp");
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String buttonValue = request.getParameter("Submit");
		System.out.println("Button VALUE THAT WAS CLICKED IS  ---- " + buttonValue);
		String errorString = null;
		List<DateLog> list = null;
		if (buttonValue.equalsIgnoreCase("getReport")) {
			String month = request.getParameter("month");
			String year = request.getParameter("year");

			try {
				list = DBUtils.bookingFeeForMonth(conn, month, year);
			} catch (SQLException e) {
				System.out.println(
						"ERROR LINE 66 SALESREPORTSESERVELET MONTH/YEAR TRY CATCH!!!!!!!!!!!!" + month + " " + year);
				e.printStackTrace();
				errorString = e.getMessage();
			}

			request.setAttribute("errorString", errorString);
			// request.setAttribute("employeeList", list);
			request.setAttribute("salesReportForAParticularMonth", list);
			request.setAttribute("year", year);
			request.setAttribute("month", month);
			System.out.printf("list:", list.toString());
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/monthView.jsp");
			dispatcher.forward(request, response);
		} // if statement end
			// Forward to /WEB-INF/views/productListView.jsp
		else if (buttonValue.equalsIgnoreCase("GetDate")) {
			System.out.println("----------------------------");

			Date date = null;
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dateValue"));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				errorString="Please enter a date first!"; 
				request.setAttribute("errorString", errorString);
	           
				// Go back to Page
				RequestDispatcher dispatcher //
	                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/salesReportByMonthView.jsp");
	 
	            dispatcher.forward(request, response);

				e1.printStackTrace();
			}
	
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			String dateStr = dateFormat.format(date);
			
			
			System.out.println("THE DATE IS CURRENTLY " + dateStr);

			try {
				list = DBUtils.dateByDate(conn, dateStr);
			} catch (SQLException e) {
				System.out.println(
						"ERROR LINE particulardatebydate serverlet MONTH/YEAR TRY CATCH!!!!!!!!!!!!" + dateFormat);
				e.printStackTrace();
				errorString = e.getMessage();
			}

			request.setAttribute("errorString", errorString);
			request.setAttribute("dateRequested", dateStr);
			request.setAttribute("dateList", list);

			System.out.printf("list:", list.toString());
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/fullDateView.jsp");
			dispatcher.forward(request, response);

		} else {

			String customerSSN = request.getParameter("ssn");
			System.out.println("GET REPORT BY CUSTOMER SSN WAS CHOOSEN WHERE SSN =" + customerSSN);
			if (customerSSN != null || !customerSSN.isEmpty()) {
				try {
					list = DBUtils.getDateByCustomerSSN(conn, customerSSN);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(list.isEmpty()|| list.equals(null)){
					errorString="This person is lonely";
					request.setAttribute("errorString", errorString);
					
				}
				String redactedSSN = "XXXX-XX-" + customerSSN.substring(7);
				request.setAttribute("errorString", errorString);
				request.setAttribute("personSSN", redactedSSN);
				request.setAttribute("dateList", list);
				RequestDispatcher dispatcher = request.getServletContext()
						.getRequestDispatcher("/WEB-INF/views/fullDateView.jsp");
				dispatcher.forward(request, response);

			} else {
				errorString="Please enter a SSN!"; 
				request.setAttribute("errorString", errorString);
	           
				// Go back to Page
				RequestDispatcher dispatcher //
	                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/salesReportByMonthView.jsp");
	 
	            dispatcher.forward(request, response);

			}
		}

		// RequestDispatcher dispatcher =
		// request.getServletContext().getRequestDispatcher("/WEB-INF/views/monthView.jsp");
		// dispatcher.forward(request, response);
		// response.sendRedirect(request.getContextPath() +
		// "/SalesReportByMonth");
	}

}
