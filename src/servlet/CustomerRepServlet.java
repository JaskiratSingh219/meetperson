package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Account;
import beans.DateLog;
import beans.Employee;
import beans.Person;
import beans.Profile;
import utils.DBUtils;
import utils.MyUtils;

/**
 * Servlet implementation class customerRep
 */
@WebServlet(urlPatterns = { "/customerRep" })
public class CustomerRepServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CustomerRepServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection conn = MyUtils.getStoredConnection(request);
		String errorString = null;
		List<DateLog> list = null;

		// Store info in request attribute, before forward to views
		request.setAttribute("errorString", errorString);

		// System.out.printf("list:", list.toString());

		// Forward to /WEB-INF/views/productListView.jsp
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String buttonValue = request.getParameter("Submit");
		boolean tempVar = true;

		String value = request.getParameter("selectProfileButton");
		System.out.println(value + " THE PROFILE PICKED WAS HAHAHAHHAHA");
		System.out.println("Button VALUE THAT WAS CLICKED IS IN COPY  ---- " + buttonValue);
		if (value != null) {
			selectCustomerProfileAction(request, response, conn);

		}

		else if (buttonValue.equalsIgnoreCase("get customer")) {
			getCustomerAction(request, response, conn);

		}
		//
		else if (buttonValue.equalsIgnoreCase("get mailing list")) {
			getMailingListAction(request, response, conn);
		} else if (buttonValue.equalsIgnoreCase("Update Information")) {

			getUpdateInformationAction(request, response, conn);

		} else if (buttonValue.equalsIgnoreCase("get profiles")) {
			getCustomerProfileAction(request, response, conn);
		} else if (buttonValue.equalsIgnoreCase("update profile information")) {
			System.out.println("HELLO ANYONE OUT THERE>>>>>>>");
			getUpdateProfileInformation(request, response, conn);
		}
		else if(buttonValue.equalsIgnoreCase("spy on employee")){
		    String errorString = null;
	        List<Employee> list = null;
	        try {
	            list = DBUtils.queryEmployee(conn);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	        // Store info in request attribute, before forward to views
	        request.setAttribute("errorString", errorString);
	        request.setAttribute("customerRepEmployeeList", list);
	        
	        System.out.printf("list:", list.toString());
	         
	        // Forward to /WEB-INF/views/productListView.jsp
	        RequestDispatcher dispatcher = request.getServletContext()
	                .getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");
	        dispatcher.forward(request, response);
	    
			
		}

		else {

			selectCustomerAction(request, response, conn);

		}

	}

	private static void getCustomerAction(HttpServletRequest request, HttpServletResponse response, Connection conn)
			throws ServletException, IOException {
		String errorString = null;
		List<Person> list = null;
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		System.out.println("HELLO WORLD MY NAME IS FIRST AND LAST NAME");

		if (firstName.equals(null) || lastName.equals(null) || firstName.isEmpty() || lastName.isEmpty()) {
			errorString = "Enter In Both First and Last Name Field!";
			request.setAttribute("errorString", errorString);

			// Go back to Page
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

			dispatcher.forward(request, response);
		}

		try {
			list = DBUtils.getCustomerByFirstLast(conn, firstName, lastName);
			System.out.println("QUERY COMPLETED ----------");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (list.equals(null) || list.isEmpty()) {
			errorString = firstName + "," + lastName + " Does Not Exist! How dare they not use our website...";
			request.setAttribute("errorString", errorString);
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

			dispatcher.forward(request, response);

		}
		request.setAttribute("errorString", errorString);
		request.setAttribute("customerQueryResultList", list);
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

		dispatcher.forward(request, response);

	}

	private static void getMailingListAction(HttpServletRequest request, HttpServletResponse response, Connection conn)
			throws ServletException, IOException {
		String errorString = null;
		List<Person> list = null;

		try {
			list = DBUtils.getCustomerList(conn);
			System.out.println(" THE LIST IS =" + list);
			System.out.println("QUERY COMPLETED IN ELSE STATEMENT ----------");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("QUERY COMPLETED IN ELSE STATEMENT 3----------");
		if (list.equals(null) || list.isEmpty()) {
			errorString = "Looked Like We Are Bankrupted..... :[";
			request.setAttribute("errorString", errorString);
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

			dispatcher.forward(request, response);

		}
		System.out.println("QUERY COMPLETED IN ELSE STATEMENT 4----------");
		request.setAttribute("errorString", errorString);
		request.setAttribute("customersList", list);

		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

		dispatcher.forward(request, response);

	}

	private static void getUpdateInformationAction(HttpServletRequest request, HttpServletResponse response,
			Connection conn) throws ServletException, IOException {
		String errorString = null;
		String ssn = (String) request.getParameter("ssn");
		String password = (String) request.getParameter("password");
		String firstName = (String) request.getParameter("firstName");
		String lastName = (String) request.getParameter("lastName");
		String street = (String) request.getParameter("street");
		String city = (String) request.getParameter("city");
		String state = (String) request.getParameter("state");
		String zipcode = (String) request.getParameter("zipcode");
		String email = (String) request.getParameter("email");
		String telephone = (String) request.getParameter("telephone");
		double cardNumber = Double.parseDouble(request.getParameter("cardNumber"));

		Person p = new Person(ssn, password, firstName, lastName, street, city, state, zipcode, email, telephone);
		Account account = null;
		try {
			DBUtils.updateCustomerInformation(conn, p);
			DBUtils.updateCustomerAccount(conn, cardNumber, ssn);
			account = DBUtils.getCustomerAccount(conn, ssn);
			System.out.println("QUERY COMPLETED IN ELSE STATEMENT ----------");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		errorString = "Information Updated! :)";
		request.setAttribute("errorString", errorString);
		request.setAttribute("selectedCustomer", p);
		request.setAttribute("customerAccount", account);

		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

		dispatcher.forward(request, response);

	}

	private static void selectCustomerAction(HttpServletRequest request, HttpServletResponse response,
			Connection conn) {
		String errorString = null;

		String ssn = request.getParameter("Submit");
		System.out.println("THE USER CHOOSEN HAS SSN OF " + ssn);
		Person p = null;
		Account account = null;
		try {
			p = DBUtils.getSpecificCustomer(conn, ssn);
			account = DBUtils.getCustomerAccount(conn, ssn);

			System.out.println("QUERY COMPLETED IN ELSE STATEMENT ----------");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("errorString", errorString);
		request.setAttribute("selectedCustomer", p);
		request.setAttribute("customerAccount", account);

		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

		try {
			dispatcher.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void getUpdateProfileInformation(HttpServletRequest request, HttpServletResponse response,
			Connection conn) throws ServletException, IOException {
		String errorString = null;
		String profileID = request.getParameter("customerProfileID");
		System.out.println("<--- SERVLET PROFILE ID IS !!!!! " + profileID);
		String ownerSSN = request.getParameter("ownerSSN");
		int age = Integer.parseInt(request.getParameter("age").replaceAll("\\s+", ""));
		int datingAgeRangeStart = Integer.parseInt(request.getParameter("datingAgeRangeStart"));
		int datingAgeRangeEnd = Integer.parseInt(request.getParameter("datingAgeRangeEnd"));
		int datingGeoRange = Integer.parseInt(request.getParameter("geoRange"));
		String gender = request.getParameter("geneder");
		String temp = request.getParameter("hobbies").replaceAll("\\[", "").replaceAll("\\]", "");
		ArrayList<String> hobbies = new ArrayList<String>(Arrays.asList(temp.split(",")));
		double height = Double.parseDouble(request.getParameter("height"));
		int weight = Integer.parseInt(request.getParameter("weight"));
		String color = request.getParameter("color");
		// String creationDate = request.getParameter("creationDate");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String creationDateString = request.getParameter("creationDate");
		Date creationDate=null;
		try {
			creationDate = formatter.parse(creationDateString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		Date lastModDate = new Date();
		System.out.println("CREATION DATE STRING IS " + creationDateString);
		Profile profile = new Profile(profileID, ownerSSN, age, datingAgeRangeStart, datingAgeRangeEnd, datingGeoRange,
				gender, hobbies, height, weight, color, creationDate, lastModDate);
		Person p = null;
		Account account = null;
		try {
			p = DBUtils.getSpecificCustomer(conn, ownerSSN);
			System.out.println("CRS getupdateprofileInformation" + ownerSSN);
			DBUtils.updateProfileForCustomer(conn, profile);
			account = DBUtils.getCustomerAccount(conn, ownerSSN);

			System.out.println("QUERY COMPLETED IN ELSE STATEMENT ----------");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		errorString = "Profile Information Updated! :)";
		request.setAttribute("errorString", errorString);
		request.setAttribute("selectedCustomer", p);
		request.setAttribute("selectedProfile", profile);
		request.setAttribute("customerAccount", account);
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");

		dispatcher.forward(request, response);

	}

	private static void getCustomerProfileAction(HttpServletRequest request, HttpServletResponse response,
			Connection conn) throws ServletException, IOException {
		String ssn = request.getParameter("ssn");
		System.out.println("CUSTOMER SSN THAT NEEDS PROFILES IS " + ssn);

		List<Profile> list = null;
		Person p = null;
		Account account = null;
		try {
			p = DBUtils.getSpecificCustomer(conn, ssn);
			account = DBUtils.getCustomerAccount(conn, ssn);
			list = DBUtils.getCustomerProfile(conn, ssn);
			System.out.println(" THE LIST IS =" + list);
			System.out.println("QUERY COMPLETED IN ELSE STATEMENT ----------");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("selectedCustomer", p);
		request.setAttribute("customerAccount", account);
		request.setAttribute("customerProfileList", list);
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");
		dispatcher.forward(request, response);

	}

	private static void selectCustomerProfileAction(HttpServletRequest request, HttpServletResponse response,
			Connection conn) throws ServletException, IOException {
		String errorString = null;
		Profile profile = null;
		Person p = null;
		String profileID = request.getParameter("selectProfileButton");
		Account account = null;

		try {
			profile = DBUtils.getCustomerProfileByID(conn, profileID);
			String ssn = profile.getOwnerSSN();
			p = DBUtils.getSpecificCustomer(conn, ssn);
			account = DBUtils.getCustomerAccount(conn, ssn);
			System.out.println("QUERY COMPLETED CATCH STATEMENT ----------");
			System.out.println(p.getSsn() + " THER PERSON THAT WAS PICKED SSN IS");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		request.setAttribute("errorString", errorString);
		request.setAttribute("selectedCustomer", p);
		request.setAttribute("customerAccount", account);
		request.setAttribute("selectedProfile", profile);

		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/customerRepView.jsp");
		dispatcher.forward(request, response);

	}
}
