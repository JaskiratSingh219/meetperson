package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DateLog;
import beans.Employee;
import beans.Favorite;
import beans.Like;
import beans.Person;
import beans.Profile;
import beans.User;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/requestDate" })
public class DateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String ssn;

	public DateServlet() {

	}

	 @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        Connection conn = MyUtils.getStoredConnection(request);
	 
	        String profileId = (String) request.getParameter("profileId");
	        HttpSession session = request.getSession();
	        Person person = MyUtils.getLoginedPerson(session);
	        ssn = person.getSsn();
	        
	        List<Profile> profiles = null;
	        String errorString = null;
	 
	        try {
	            profiles = DBUtils.getProfilesBySSN(conn, ssn);
	        } catch (SQLException | ParseException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	 
	        // If no error.
	        // The product does not exist to edit.
	        // Redirect to productList page.
	        if (errorString != null && profiles == null) {
	            response.sendRedirect(request.getServletPath() + "/Account");
	            return;
	        }

	        // Store errorString in request attribute, before forward to views.
	        request.setAttribute("errorString", errorString);
	        request.setAttribute("profiles", profiles);
	        request.setAttribute("profileId", profileId);
	 
	        RequestDispatcher dispatcher = request.getServletContext()
	                .getRequestDispatcher("/WEB-INF/views/dateView.jsp");
	        dispatcher.forward(request, response);
	 
	    }
	 
	    // After the user modifies the product information, and click Submit.
	    // This method will be executed.
	    @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        Connection conn = MyUtils.getStoredConnection(request);
	 
	        String profile1 = (String) request.getParameter("profile1");
	        String profile2 = (String) request.getParameter("profile2");
	        String ds = (String) request.getParameter("date");
	        
	        ds = ds.replaceAll("T", " ");
	        Date d = null;
			try {
				d = (Date) new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(ds);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dt = a.format(d);
	        
	        String location = (String) request.getParameter("location");
	        double bookingFee = 50;
	        String comments = (String) request.getParameter("comments");
	        
	        DateLog dl = new DateLog();
	        
	        dl.setProfile1(profile1);
	        dl.setProfile2(profile2);
	        dl.setDate_time(dt);
	        dl.setLocation(location);
	        dl.setBookingFee(bookingFee);
	        dl.setComments(comments);
	        
	  
	        String errorString = null;
	 
	        try {
	            DBUtils.addDate(conn, dl);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	        
	        // Store infomation to request attribute, before forward to views.
	        request.setAttribute("errorString", errorString);
	        request.setAttribute("dl", dl);
	 
	        // If error, forward to Edit page.
	        if (errorString != null) {
	            RequestDispatcher dispatcher = request.getServletContext()
	                    .getRequestDispatcher("/WEB-INF/views/dateView.jsp");
	            dispatcher.forward(request, response);
	        }
	        // If everything nice.
	        // Redirect to the product listing page.
	        else {
	            response.sendRedirect(request.getContextPath() + "/Account?ssn=" + ssn );
	        }
	    }

}
