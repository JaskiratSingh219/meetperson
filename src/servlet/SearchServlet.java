package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DateLog;
import beans.Favorite;
import beans.Profile;
import beans.User;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/search" })
public class SearchServlet extends HttpServlet {
	String ssn;
	private static final long serialVersionUID = 1L;

	public SearchServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ssn = (String) request.getParameter("ssn");

		// Store info in request attribute, before forward to views
		// request.setAttribute("errorString", errorString);
		request.setAttribute("ssn", ssn);

		// Forward to /WEB-INF/views/productListView.jsp
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/searchView.jsp");
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String ageStart = request.getParameter("ageStart");
		String ageEnd = request.getParameter("ageEnd");
		String gender = request.getParameter("gender");
		String hobbies = request.getParameter("hobbies");
		String height = request.getParameter("height");
		String hairColor = request.getParameter("hairColor");

		if (ageStart.equals(""))
			ageStart = "18";
		if (ageEnd.equals(""))
			ageEnd = "100";
		if (gender.equals(""))
			gender = "%";
		if (hobbies.equals("")) {
			hobbies = "%";
		} else {
			hobbies = "%" + hobbies + "%";
		}
		if (height.equals(""))
			height = "4.0";
		if (hairColor.equals(""))
			hairColor = "%";
		
		System.out.println(ageStart);

		int as = Integer.parseInt(ageStart);
		int ae = Integer.parseInt(ageEnd);
		double he = Double.parseDouble(height);

		List<Profile> profiles = null;
		String errorString = null;

		try {
			profiles = DBUtils.searchProfiles(conn, as, ae, gender, hobbies, he, hairColor);
		} catch (SQLException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorString = e.getMessage();
		}
		
		 request.setAttribute("errorString", errorString);
		 request.setAttribute("profiles", profiles);

		if (errorString != null) {
			RequestDispatcher dispatcher = request.getServletContext()
					.getRequestDispatcher("/WEB-INF/views/browseView.jsp");
			dispatcher.forward(request, response);
		}
		// If everything nice.
		// Redirect to the product listing page.
		else {
			response.sendRedirect(request.getContextPath() + "/browse?ssn=" + ssn);
		}
	}

}
