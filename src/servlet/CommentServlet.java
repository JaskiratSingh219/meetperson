package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.DateFormatter;

import beans.DateLog;
import beans.Employee;
import beans.Person;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	/**
	 * 
	 */
//	private static final long serialVersionUID = 1L;
	String id1;
	String id2;
	String date;
	String comments;
	
	public CommentServlet(){
		
	}
	
	 @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        Connection conn = MyUtils.getStoredConnection(request);
	        
	        id1 = request.getParameter("pd1");
	        id2 = request.getParameter("pd2");
	        date = request.getParameter("pdt");
	        comments = request.getParameter("pdc");
	        
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	        
			Date dt;
			String s = "aa";
			try {
				dt = format.parse(date);
				s = format.format(dt);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	        System.out.println(s);
	        
	        DateLog dl = new DateLog();
	        dl.setProfile1(id1);
	        dl.setProfile2(id2);
	        dl.setDate_time(s);
	        dl.setComments(comments);
	        
	        request.setAttribute("dl", dl);
	        RequestDispatcher dispatcher = request.getServletContext()
	                .getRequestDispatcher("/WEB-INF/views/addCommentsView.jsp");
	        dispatcher.forward(request, response);       
	        
	 }
	 
	 @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        Connection conn = MyUtils.getStoredConnection(request);
	 
//	        System.out.println(id1);
////	        String id2 = request.getParameter("pd2");
////	        String date = request.getParameter("pdt");
////	        String comments = request.getParameter("pdc");
	        String addedComments = request.getParameter("comments");
	        comments = comments + "\n" + addedComments;
	        
	        HttpSession session = request.getSession();
	        Person person = MyUtils.getLoginedPerson(session);
	        String ssn = person.getSsn();
	 
	        String errorString = null;
	 
	        try {
	            DBUtils.updateComments(conn, id1, id2, date, comments);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	        // Store infomation to request attribute, before forward to views.
	        request.setAttribute("errorString", errorString);
//	        request.setAttribute("employee", employee);
	 
	        // If error, forward to Edit page.
	        if (errorString != null) {
	            RequestDispatcher dispatcher = request.getServletContext()
	                    .getRequestDispatcher("/WEB-INF/views/addCommentsView.jsp");
	            dispatcher.forward(request, response);
	        }
	        // If everything nice.
	        // Redirect to the product listing page.
	        else {
	            response.sendRedirect(request.getContextPath() + "/Account?ssn=" + ssn);
	        }
	    }

}
