package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DateLog;
import beans.Favorite;
import beans.Profile;
import beans.User;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/mostActiveProfiles" })
public class MostActiveProfilesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MostActiveProfilesServlet() {

	}

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        
        String ssn = (String)request.getParameter("ssn");
 
        String errorString = null;
        List<User> user = null;
        try {
            user = DBUtils.mostActiveProfiles(conn);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        // Store info in request attribute, before forward to views
        request.setAttribute("errorString", errorString);
        request.setAttribute("profiles", user);
         
        // Forward to /WEB-INF/views/productListView.jsp
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/mostActiveProfilesView.jsp");
        dispatcher.forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
