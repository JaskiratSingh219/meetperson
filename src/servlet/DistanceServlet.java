package servlet;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import beans.Employee;
import beans.Person;
import beans.User;
import beans.UserAccount;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/dist" })
public class DistanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DistanceServlet() {
		super();
	}

	// Show Login page.
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//JSONObject json = new JSONObject(IOUtils.toString(new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins=Brentwood+NY&destinations=Smithtown+NY"), Charset.forName("UTF-8")));
//		
//		System.out.println(json.toString());
		// Map<String, String> parameters = new HashMap<>();
		// parameters.put("origins", "Brentwood+NY");
		// parameters.put("destinations", "Smithtown+NY");

		// Forward to /WEB-INF/views/loginView.jsp
		// (Users can not access directly into JSP pages placed in WEB-INF)
		RequestDispatcher dispatcher //
				= this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");

		dispatcher.forward(request, response);

	}

	// When the user enters userName & password, and click Submit.
	// This method will be executed.
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// String userName = request.getParameter("userName");
		// String password = request.getParameter("password");
		// String rememberMeStr = request.getParameter("rememberMe");
		// boolean remember = "Y".equals(rememberMeStr);
		//
		// Person person = null;
		// boolean hasError = false;
		// String errorString = null;
		// Connection conn = null;
		// String type = "";
		//
		// if (userName == null || password == null || userName.length() == 0 ||
		// password.length() == 0) {
		// hasError = true;
		// errorString = "Required username and password!";
		// } else {
		// conn = MyUtils.getStoredConnection(request);
		// try {
		// // Find the user in the DB.
		// person = DBUtils.findPerson(conn, userName, password);
		//
		// if (person == null) {
		// hasError = true;
		// errorString = "User Name or password invalid";
		// }
		// } catch (SQLException e) {
		// e.printStackTrace();
		// hasError = true;
		// errorString = e.getMessage();
		// }
		// }
		// // If error, forward to /WEB-INF/views/login.jsp
		// if (hasError) {
		// Person user = new User();
		// user.setEmail(userName);
		// user.setPassword(password);
		//
		// // Store information in request attribute, before forward.
		// request.setAttribute("errorString", errorString);
		// request.setAttribute("user", user);
		//
		// // Forward to /WEB-INF/views/login.jsp
		// RequestDispatcher dispatcher //
		// =
		// this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
		//
		// dispatcher.forward(request, response);
		// }
		// // If no error
		// // Store user information in Session
		// // And redirect to userInfo page.
		// else {
		// HttpSession session = request.getSession();
		// Employee employee = null;
		// try {
		// employee = DBUtils.findEmployee(conn, person.getSsn());
		// if (employee == null) {
		// type = "user";
		// } else {
		// type = employee.getRole();
		// }
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// MyUtils.storeLoginedUser(session, person, type);
		//
		// // If user checked "Remember me".
		// if (remember) {
		// MyUtils.storeUserCookie(response, person);
		// }
		// // Else delete cookie.
		// else {
		// MyUtils.deleteUserCookie(response);
		// }
		//
		// if (employee == null)
		// response.sendRedirect(request.getContextPath() + "/Account?ssn=" +
		// person.getSsn());
		// }
	}

	// public static String getParamsString(Map<String, String> params) throws
	// UnsupportedEncodingException {
	// StringBuilder result = new StringBuilder();
	//
	// for (Map.Entry<String, String> entry : params.entrySet()) {
	// result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
	// result.append("=");
	// result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
	// result.append("&");
	// }
	//
	// String resultString = result.toString();
	// return resultString.length() > 0 ? resultString.substring(0,
	// resultString.length() - 1) : resultString;
	// }

}