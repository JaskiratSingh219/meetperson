package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DateLog;
import beans.Employee;
import beans.Favorite;
import beans.Like;
import beans.Person;
import beans.Profile;
import beans.User;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/like" })
public class LikeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LikeServlet() {

	}

	 @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        Connection conn = MyUtils.getStoredConnection(request);
	 
	        String profileId = (String) request.getParameter("profileId");
	        HttpSession session = request.getSession();
	        Person person = MyUtils.getLoginedPerson(session);
	        String ssn = person.getSsn();
	        
	        List<Profile> profiles = null;
	        String errorString = null;
	 
	        try {
	            profiles = DBUtils.getProfilesBySSN(conn, ssn);
	        } catch (SQLException | ParseException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	 
	        // If no error.
	        // The product does not exist to edit.
	        // Redirect to productList page.
	        if (errorString != null && profiles == null) {
	            response.sendRedirect(request.getServletPath() + "/Account");
	            return;
	        }

	        // Store errorString in request attribute, before forward to views.
	        request.setAttribute("errorString", errorString);
	        request.setAttribute("profiles", profiles);
	        request.setAttribute("profileId", profileId);
	 
	        RequestDispatcher dispatcher = request.getServletContext()
	                .getRequestDispatcher("/WEB-INF/views/likeView.jsp");
	        dispatcher.forward(request, response);
	 
	    }
	 
	    // After the user modifies the product information, and click Submit.
	    // This method will be executed.
	    @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        Connection conn = MyUtils.getStoredConnection(request);
	 
	        String liker = (String) request.getParameter("liker");
	        String likee = (String) request.getParameter("likee");
	        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	        
	        long now = System.currentTimeMillis();
	        Date date = new Date(now);
	        dateFormat.format(date);
	        
	        Like like = new Like(liker, likee, date);
	  
	        String errorString = null;
	 
	        try {
	            DBUtils.addLike(conn, like);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	        // Store infomation to request attribute, before forward to views.
	        request.setAttribute("errorString", errorString);
//	        request.setAttribute("employee", employee);
	 
	        // If error, forward to Edit page.
	        if (errorString != null) {
	            RequestDispatcher dispatcher = request.getServletContext()
	                    .getRequestDispatcher("/WEB-INF/views/likeView.jsp");
	            dispatcher.forward(request, response);
	        }
	        // If everything nice.
	        // Redirect to the product listing page.
	        else {
	            response.sendRedirect(request.getContextPath() + "/Account");
	        }
	    }

}
