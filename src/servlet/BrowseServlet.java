package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.DateLog;
import beans.Favorite;
import beans.Profile;
import beans.User;
import utils.DBUtils;
import utils.MyUtils;

@WebServlet(urlPatterns = { "/browse" })
public class BrowseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public BrowseServlet() {

	}

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        
        String ssn = (String)request.getParameter("ssn");
 
        String errorString = null;
        List<Profile> profiles = null;
        try {
            profiles = DBUtils.browseProfiles(conn, ssn);
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        
        // Store info in request attribute, before forward to views
        request.setAttribute("errorString", errorString);
        request.setAttribute("profiles", profiles);
         
        // Forward to /WEB-INF/views/productListView.jsp
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/browseView.jsp");
        dispatcher.forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
