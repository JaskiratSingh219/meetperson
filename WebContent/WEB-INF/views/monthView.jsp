<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Report List</title>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_managerMenu.jsp"></jsp:include>

	<h3>Sales List</h3>


	<p style="color: red;">${errorString}</p>
	<p>SALES REPORT FOR  ${month} / ${year}</p>
	<c:if test="${empty salesReportForAParticularMonth}">
		<h2>EMPTY LIST: NO RESULTS FOUND</h2>
	</c:if>
	
	<table border="1" cellpadding="5" cellspacing="1">
		<tr>
			<th>Date_Time</th>
			<th>BookingFee</th>


		</tr>
		<c:forEach items="${salesReportForAParticularMonth}" var="date">
			<tr>
				<td>${date.date_time}</td>
				<td>${date.bookingFee}</td>

			</tr>
		</c:forEach>

	</table>
	

	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>