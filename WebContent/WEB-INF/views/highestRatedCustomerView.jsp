<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Highest Rated Customers</title>
 </head>
 <body bgcolor="#B0F8FD">
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_managerMenu.jsp"></jsp:include>
 
    <h3>Highest Rated Customers</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>Customer ID</th>
          <th>Rating</th>
       </tr>
       <c:forEach items="${userList}" var="user" >
          <tr>
             <td>${user.ssn}</td>
             <td>${user.rating}</td>
          </tr>
       </c:forEach>
    </table>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>