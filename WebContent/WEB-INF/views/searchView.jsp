<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Employee</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h3>Edit Employee</h3>

	<p style="color: red;">${errorString}</p>
		<form method="POST"
			action="${pageContext.request.contextPath}/search">
			<table border="0">
				<tr>
					<td>Age Start</td>
					<td><input type="text" name="ageStart"/></td>
				</tr>
				<tr>
					<td>Age End</td>
					<td><input type="text" name="ageEnd"/></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td><input type="text" name="gender" /></td>
				</tr>
				<tr>
					<td>Hobbies</td>
					<td><input type="text" name="hobbies" /></td>
				</tr>
				<tr>
					<td>Height</td>
					<td><input type="text" name="height"/></td>
				</tr>
				<tr>
					<td>Hair Color</td>
					<td><input type="text" name="hairColor" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /> <a
						href="${pageContext.request.contextPath}/search?ssn=${ssn}">Cancel</a>
					</td>
				</tr>
			</table>
		</form>

	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>