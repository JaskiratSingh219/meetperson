<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Most Profitable Users</title>
 </head>
 <body bgcolor="#B0F8FD">
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_managerMenu.jsp"></jsp:include>
 
    <h3>Users with the Most Total Revenue</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>Profile</th>
          <th>Total Revenue</th>
       </tr>
       <c:forEach items="${users}" var="user" >
          <tr>
             <td>${user.ssn}</td>
             <td>${user.rating}</td>
          </tr>
       </c:forEach>
    </table>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>