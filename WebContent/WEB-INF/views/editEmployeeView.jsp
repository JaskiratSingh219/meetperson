<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Edit Employee</title>
   </head>
   <body bgcolor="#B0F8FD">
 
      <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
 
      <h3>Edit Employee</h3>
 
      <p style="color: red;">${errorString}</p>
 
      <c:if test="${not empty employee}">
         <form method="POST" action="${pageContext.request.contextPath}/editEmployee">
            <input type="hidden" name="ssn" value="${employee.ssn}" />
            <table border="0">
            
               <tr>
                  <td>SSN</td>
                  <td style="color:red;">${employee.ssn}</td>
               </tr>
               <tr>
                  <td>Password</td>
                  <td><input type="text" name="password" value="${employee.password}" /></td>
               </tr>
               <tr>
                  <td>First Name</td>
                  <td><input type="text" name="firstName" value="${employee.firstName}" /></td>
               </tr>
               <tr>
					<td>LastName</td>
					<td><input type="text" name="lastName" value="${employee.lastName}" /></td>
				</tr>
				<tr>
					<td>Street</td>
					<td><input type="text" name="street" value="${employee.street}" /></td>
				</tr>
				<tr>
					<td>City</td>
					<td><input type="text" name="city" value="${employee.city}" /></td>
				</tr>
				<tr>
					<td>State</td>
					<td><input type="text" name="state" value="${employee.state}" /></td>
				</tr>
				<tr>
					<td>Zipcode</td>
					<td><input type="text" name="zipcode" value="${employee.zipcode}" /></td>
				</tr>
				<tr>
					<td>E-Mail</td>
					<td><input type="text" name="email" value="${employee.email}" /></td>
				</tr>
				<tr>
					<td>Telephone</td>
					<td><input type="text" name="telephone" value="${employee.telephone}" /></td>
				</tr>
				<tr>
					<td>Role</td>
					<td><input type="text" name="role" value="${employee.role}" /></td>
				</tr>
				<tr>
					<td>Start Date</td>
					<td><input type="date" name="startDate" value="${employee.startDate}" /></td>
				</tr>
				<tr>
					<td>Hourly Rate</td>
					<td><input type="text" name="hourlyRate" value="${employee.hourlyRate}" /></td>
				</tr>
               <tr>
                  <td colspan = "2">
                      <input type="submit" value="Submit" />
                      <a href="${pageContext.request.contextPath}/employeeList">Cancel</a>
                  </td>
               </tr>
            </table>
         </form>
      </c:if>
 
      <jsp:include page="_footer.jsp"></jsp:include>
 
   </body>
</html>