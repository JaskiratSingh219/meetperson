<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Report List</title>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_managerMenu.jsp"></jsp:include>

	<h3>Sales List</h3>


	<p style="color: red;">${errorString}</p>
	<p>SALES REPORT FOR ${dateRequested} ${personSSN}</p>
	<c:if test="${empty dateList}">
		<h2>EMPTY LIST: NO RESULTS FOUND</h2>
	</c:if>
	<table border="1" cellpadding="5" cellspacing="1">
		<tr>
			<th>Profile1</th>
			<th>Profile2</th>
			<th>Customer Rep</th>
			<th>Date_Time</th>
			<th>Location</th>
			<th>BookingFee</th>
			<th>Comments</th>
			<th>User1Rating</th>
			<th>User2Rating</th>



		</tr>

		<c:forEach items="${dateList}" var="date">
			<tr>
				<td>${date.profile1}</td>
				<td>${date.profile2}</td>
				<td>${date.custRep}</td>
				<td>${date.date_time}</td>
				<td>${date.location}</td>
				<td>${date.bookingFee}</td>
				<td>${date.comments}</td>
				<td>${date.user1Rating}</td>
				<td>${date.user2Rating}</td>


			</tr>
		</c:forEach>


	</table>


	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>