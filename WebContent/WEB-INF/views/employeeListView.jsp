<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Employee List</title>
 </head>
 <body bgcolor="#B0F8FD">
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_managerMenu.jsp"></jsp:include>
 
    <h3>Employee List</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>SSN</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Street</th>
          <th>City</th>
          <th>State</th>
          <th>Zipcode</th>
          <th>Email</th>
          <th>Telephone</th>
          <th>Role</th>
          <th>Start Date</th>
          <th>Hourly Rate</th>
          <th>Edit</th>
          <th>Delete</th>
       </tr>
       <c:forEach items="${employeeList}" var="user" >
          <tr>
             <td>${user.ssn}</td>
             <td>${user.firstName}</td>
             <td>${user.lastName}</td>
             <td>${user.street}</td>
             <td>${user.city}</td>
             <td>${user.state}</td>
             <td>${user.zipcode}</td>
             <td>${user.email}</td>
             <td>${user.telephone}</td>
             <td>${user.role}</td>
             <td>${user.startDate}</td>
             <td>${user.hourlyRate}</td>
             <td>
                <a href="editEmployee?ssn=${user.ssn}">Edit</a>
             </td>
             <td>
                <a href="deleteEmployee?ssn=${user.ssn}">Delete</a>
             </td>
          </tr>
       </c:forEach>
    </table>
 
    <a href="createEmployee" >Create Employee</a>
    
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>