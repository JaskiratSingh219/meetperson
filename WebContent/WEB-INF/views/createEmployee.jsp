<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <title>Add Employee</title>
   </head>
   <body bgcolor="#B0F8FD">
    
      <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_managerMenu.jsp"></jsp:include>
       
      <h3>New Employee</h3>
       
      <p style="color: red;">${errorString}</p>
       
      <form method="POST" action="${pageContext.request.contextPath}/man/createEmployee">
         <table border="0">
            <tr>
               <td>SSN</td>
               <td><input type="text" name="ssn" placeholder="XXX-XX-XXXX"
						pattern="\d{3}-?\d{2}-?\d{4}" required/></td>
            </tr>
            <tr>
               <td>Password</td>
               <td><input type="text" name="password" required></td>
            </tr>
            <tr>
               <td>FirstName</td>
               <td><input type="text" name="firstName" required/></td>
            </tr>
            <tr>
               <td>LastName</td>
               <td><input type="text" name="lastName" required/></td>
            </tr>
            <tr>
               <td>Street</td>
               <td><input type="text" name="street" required/></td>
            </tr>
            <tr>
               <td>City</td>
               <td><input type="text" name="city" required/></td>
            </tr>
            <tr>
               <td>State</td>
               <td><input type="text" name="state" required/></td>
            </tr>
            <tr>
               <td>Zipcode</td>
               <td><input type="text" name="zipcode" required/></td>
            </tr>
            <tr>
               <td>E-Mail</td>
               <td><input type="text" name="email" required/></td>
            </tr>
            <tr>
               <td>Telephone</td>
               <td><input type="text" name="telephone" placeholder="XXX-XXX-XXXX"
						pattern="\d{3}-?\d{3}-?\d{4}"required/></td>
            </tr>
            <tr>
               <td>Role</td>
               <td><input type="radio" name="role" value="Manager" required/>Manager
               <input type="radio" name="role" value="Employee" />Employee</td>
            </tr>
            <tr>
               <td>Start Date</td>
               <td><input type="date" name="startDate" required/></td>
            </tr>
            <tr>
               <td>Hourly Rate (USD)</td>
               <td><input type="text" name="hourlyRate" pattern="\d+$" required/></td>
            </tr>
            
            <tr>
               <td colspan="2">                   
                   <input type="submit" value="Submit" />
                   <a href="employeeList">Cancel</a>
               </td>
            </tr>
         </table>
      </form>
       
      <jsp:include page="_footer.jsp"></jsp:include>
       
   </body>
</html>