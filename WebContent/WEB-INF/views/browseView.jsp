<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Browse</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h2>Browse</h2>

	<p style="color: red;">${errorString}</p>
</head>
<body>

	<style>
* {
	box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
	float: left;
	width: 25%;
	padding: 10px;
}

/* Clear floats after the columns */
.row:after {
	content: "";
	display: table;
	clear: both;
}
/* Style the buttons */
.btn {
	border: none;
	outline: none;
	padding: 12px 16px;
	background-color: #f1f1f1;
	cursor: pointer;
}

.btn:hover {
	background-color: #ddd;
}

.btn.active {
	background-color: #666;
	color: white;
}
</style>

	<div id="btnContainer">
		<button class="btn" onclick="listView()">
			<i class="fa fa-bars"></i> List
		</button>
		<button class="btn active" onclick="gridView()">
			<i class="fa fa-th-large"></i> Grid
		</button>
	</div>
	<br>

	<c:forEach items="${profiles}" var="pf">
		<div class="column" style="background-color: #bbb; border: 2px solid #ccc; border-radius: 5px;">
			<h3>${pf.profileId }</h3>
			<p>${pf.age}, ${pf.gender }</p>
			<p>Looking for Ages: ${pf.datingAgeRangeStart} - ${pf.datingAgeRangeEnd}</p>
			<p><a href="viewProfile?profileId=${pf.profileId}" >View</a> 
			<a href="requestDate?profileId=${pf.profileId}" >Request a Date</a> 
			<a href="like?profileId=${pf.profileId}" >Like</a> 
			<a href="refer?profileId=${pf.profileId}" >Refer</a></p>
		</div>
	</c:forEach>

	<script>
		// Get the elements with class="column"
		var elements = document.getElementsByClassName("column");

		// Declare a loop variable
		var i;

		// List View
		function listView() {
			for (i = 0; i < elements.length; i++) {
				elements[i].style.width = "100%";
			}
		}

		// Grid View
		function gridView() {
			for (i = 0; i < elements.length; i++) {
				elements[i].style.width = "25%";
			}
		}

		/* Optional: Add active class to the current button (highlight it) */
		var container = document.getElementById("btnContainer");
		var btns = container.getElementsByClassName("btn");
		for (var i = 0; i < btns.length; i++) {
			btns[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");
				current[0].className = current[0].className.replace(" active",
						"");
				this.className += " active";
			});
		}
	</script>
</body>