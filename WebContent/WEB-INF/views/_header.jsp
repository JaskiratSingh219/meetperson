<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<div style="background: #01DFD7; height: 55px; padding: 5px;">
  <div style="float: left">
     <h1>Meet.Person</h1>
  </div>
 
  <div style="float: right; padding: 10px; text-align: right;">
 
     <!-- User store in session with attribute: loginedUser -->
     Hello <b>${loginedUser.firstName}</b>
 
  </div>
 
</div>