<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Employee</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h3>Like a Profile</h3>

	<p style="color: red;">${errorString}</p>

	<c:if test="${not empty profiles}">
		<form method="POST"
			action="${pageContext.request.contextPath}/like">
			<input type="hidden" name="likee" value="${profileId}" />
			<table border="0">
				<tr>
					<td>Liking Profile</td>
					<td style="color: red;">${profileId}</td>
				</tr>
				<tr>
					<td>Liked By</td>
					<c:forEach items="${profiles}" var="pf">
					<td><input type="radio" name="liker" value="${pf.profileId}">${pf.profileId}</td>
					</c:forEach>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /> <a
						href="${pageContext.request.contextPath}/Account">Cancel</a></td>
				</tr>
			</table>
		</form>
	</c:if>

	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>