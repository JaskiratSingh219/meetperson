<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Employee</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h3>Edit Employee</h3>

	<p style="color: red;">${errorString}</p>

	<form method="POST" action="${pageContext.request.contextPath}/comment">
		<table border="0">
			<tr>
				<td>Profile 1:</td>
				<td name="pd1" value="${dl.profile1}">${dl.profile1}</td>
			</tr>
			<tr>
				<td>Profile 2:</td>
				<td name="pd2" value="${dl.profile2}">${dl.profile2}</td>
			</tr>
			<tr>
				<td>Date/Time:</td>
				<td name="pdt" value="${dl.date_time}">${dl.date_time}</td>
			</tr>
			<tr>
				<td>Comments:</td>
				<td name="pdc" value="${dl.comments}">${dl.comments}</td>
			</tr>
			<tr>
				<td> Add Comments:</td>
				<td><input type="text" name="comments"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /> <a
					href="${pageContext.request.contextPath}/comment">Cancel</a></td>
			</tr>
		</table>
	</form>


	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>