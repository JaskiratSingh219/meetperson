<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User List</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_managerMenu.jsp"></jsp:include>

	<h3>User List</h3>

	<p style="color: red;">${errorString}</p>

	<a href="createUser">Create User</a>
	<form method="POST"
		action="${pageContext.request.contextPath}/customerRep">

		<button type="Submit" name="Submit" Value=${person.ssn}>DELETE</button>
	</form>
	<button type="Submit" name="Submit" value=${person.ssn} onclick="myFunction()">CREATE
		USER</button>
		
	<script></script>

	<table border="1" cellpadding="5" cellspacing="1">
		<tr>
			<th>SSN</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Street</th>
			<th>City</th>
			<th>State</th>
			<th>Zipcode</th>
			<th>Email</th>
			<th>Telephone</th>
			<th>Placement Policy</th>
			<th>Rating</th>
			<th>Last Accessed</th>
			<th>Customers Dated</th>
		</tr>
		<c:forEach items="${userList}" var="user">
			<tr>
				<td>${user.ssn}</td>
				<td>${user.firstName}</td>
				<td>${user.lastName}</td>
				<td>${user.street}</td>
				<td>${user.city}</td>
				<td>${user.state}</td>
				<td>${user.zipcode}</td>
				<td>${user.email}</td>
				<td>${user.telephone}</td>
				<td>${user.ppp}</td>
				<td>${user.rating}</td>
				<td>${user.lastAccessed}</td>
				<td><a href="datedUsers?ssn=${user.ssn}">Customers Dated</a></td>
			</tr>
		</c:forEach>
	</table>

	<script src="assets/_build/js/lib/particles.js"></script>

	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>