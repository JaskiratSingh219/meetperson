<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${pf.profileId }</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h2>${pf.profileId }</h2>

	<p style="color: red;">${errorString}</p>
</head>
<body>

	<style>
* {
	box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
	float: left;
	width: 33%;
	height: 300px;
	padding: 10px;
}

.hobby {
	float: left;
	width: 33%;
	padding: 15px;
	height: 300px;
}
.links {
	float: left;
	width: 33%;
	padding: 15px;
	height: 300px;
}
</style>

	<div class="column"
		style="background-color: #ccc; border: 2px solid #aaa; border-radius: 5px;">
		<p>${pf.age},${pf.gender }</p>
		<p>Looking for Ages: ${pf.datingAgeRangeStart} -
			${pf.datingAgeRangeEnd}</p>
		<p>Height: ${pf.height}</p>
		<p>Weight: ${pf.weight }</p>
		<p>Hair Color: ${pf.color }</p>
		<p>Last Active:</p>
	</div>
	<div class="hobby"
		style="background-color: #ccc; border: 2px solid #ddd; border-radius: 5px;">
		<h3>Hobbies</h3>
		<ul>
			<c:forEach items="${pf.hobbies}" var="hobby">
				<li>${hobby }</li>
			</c:forEach>
		</ul>
	</div>
	<div class="links" style="background-color: #ccc; border: 2px solid #aaa; border-radius: 5px;">
		<p>
			<a href="requestDate?profileId=${pf.profileId}">Request a Date</a>
		</p>
		<p>
			<a href="like?profileId=${pf.profileId}">Like</a>
		</p>
		<p>
			<a href="refer?profileId=${pf.profileId}">Refer</a>
		</p>
	</div>

</body>
</html>