<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CustomerRep</title>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h3>Customer Representative View</h3>


	<p style="color: red;">${errorString}</p>

	<table border="1" cellpadding="5" cellspacing="1">

		<tr>
			<th>Get Customer</th>
			<th>Produce Customer Mailing List</th>

			<th>Refer User</th>
			<th>Spy on Employee</th>

		</tr>
		<tr>
			<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/customerRep">


					First Name: <input type="text" name="firstName"
						placeholder="First Name" /> <br /> Last Name: <input type="text"
						name="lastName" placeholder="Last Name" /> <br /> <input
						type="Submit" name="Submit" Value="Get Customer" />
				</form>

			</th>
			<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/customerRep">

					<input type="Submit" name="Submit" Value="Get Mailing List" />
				</form>

			</th>

			<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/customerRep">

					<input type="Submit" name="Submit" Value="Refer User" />
				</form>

			</th>
			<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/customerRep">

					<input type="Submit" name="Submit" Value="Spy On Employee" />
				</form>

			</th>



		</tr>
	</table>
	<c:if test="${!empty customerRepEmployeeList}">
		<table border="1" cellpadding="5" cellspacing="1">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Street</th>
				<th>City</th>
				<th>State</th>
				<th>Zipcode</th>
				<th>Email</th>
				<th>Telephone</th>
				<th>Role</th>
				<th>Start Date</th>
			</tr>
			<c:forEach items="${customerRepEmployeeList}" var="user">
				<tr>
					<td>${user.firstName}</td>
					<td>${user.lastName}</td>
					<td>${user.street}</td>
					<td>${user.city}</td>
					<td>${user.state}</td>
					<td>${user.zipcode}</td>
					<td>${user.email}</td>
					<td>${user.telephone}</td>
					<td>${user.role}</td>
					<td>${user.startDate}</td>

				</tr>
			</c:forEach>
		</table>


	</c:if>
	<c:if test="${!empty customerQueryResultList}">


		<table border="1" cellpadding="5" cellspacing="1">
			<tr>
				<th>SSN</th>
				<th>Password</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Street</th>
				<th>City</th>
				<th>State</th>
				<th>Zip Code</th>
				<th>Email</th>
				<th>Telephone</th>


			</tr>
			<c:forEach items="${customerQueryResultList}" var="person">
				<tr>
					<td>${person.ssn}</td>
					<td>${person.password}</td>
					<td>${person.firstName}</td>
					<td>${person.lastName}</td>
					<td>${person.street}</td>
					<td>${person.city}</td>
					<td>${person.state}</td>
					<td>${person.zipcode}</td>
					<td>${person.email}</td>
					<td>${person.telephone}</td>
					<td>
						<form method="POST"
							action="${pageContext.request.contextPath}/customerRep">

							<button type="Submit" name="Submit" Value=${person.ssn}>SELECT</button>
						</form>
					</td>
					<td>
						<form method="POST"
							action="${pageContext.request.contextPath}/customerRep">

							<button type="Submit" name="Submit" Value=${person.ssn}>DELETE</button>
						</form>

					</td>
					<!-- <td><a href="editUser?ssn=${person.ssn}">Edit</a></td>
					<td><a href="deleteUser?ssn=${person.ssn}">Delete</a></td> -->
				</tr>
			</c:forEach>
		</table>

	</c:if>
	<!--        %%%%%%%%%%%%%%%%%%%%%%%%%%%CUSTOMER MAILING LIST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   -->
	<c:if test="${!empty customersList}">


		<table border="1" cellpadding="5" cellspacing="1">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>


			</tr>
			<c:forEach items="${customersList}" var="person">
				<tr>
					<td>${person.firstName}</td>
					<td>${person.lastName}</td>
					<td>${person.email}</td>
				</tr>
			</c:forEach>
		</table>

	</c:if>

	<!--        %%%%%%%%%%%%%%%%%%%%%%%%%SELECT CUSTOMER TO UPDATE INFO%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   -->

	<c:if test="${!empty selectedCustomer}">


		<form method="POST"
			action="${pageContext.request.contextPath}/customerRep">
			<input type="hidden" name="ssn" value="${selectedCustomer.ssn}" />
			<table border="0">

				<tr>
					<td>SSN</td>
					<td name="ssn" style="color: red;">${selectedCustomer.ssn}</td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="text" name="password"
						value="${selectedCustomer.password} " required /></td>
				</tr>
				<tr>
					<td>First Name</td>
					<td><input type="text" name="firstName"
						value="${selectedCustomer.firstName}" required /></td>
				</tr>
				<tr>
					<td>LastName</td>
					<td><input type="text" name="lastName"
						value="${selectedCustomer.lastName}" required /></td>
				</tr>
				<tr>
					<td>Street</td>
					<td><input type="text" name="street"
						value="${selectedCustomer.street}" required /></td>
				</tr>
				<tr>
					<td>City</td>
					<td><input type="text" name="city"
						value="${selectedCustomer.city}" required /></td>
				</tr>
				<tr>
					<td>State</td>
					<td><input type="text" name="state"
						value="${selectedCustomer.state}" required /></td>
				</tr>
				<tr>
					<td>Zipcode</td>
					<td><input type="text" name="zipcode"
						value="${selectedCustomer.zipcode}" required /></td>
				</tr>
				<tr>
					<td>E-Mail</td>
					<td><input type="text" name="email"
						value="${selectedCustomer.email}" required /></td>
				</tr>
				<tr>
					<td>Telephone</td>
					<td><input type="text" name="telephone"
						value="${selectedCustomer.telephone}" required /></td>
				</tr>
				<tr>
					<td>Card Number</td>
					<td><input type="number" name="cardNumber"
						value="${customerAccount.cardNumber}" required /></td>
				</tr>
				<tr>
					<td>Account Number</td>
					<td><input type="hidden" name="accountNumber"
						value="${customerAccount.accountNumber}" />${customerAccount.accountNumber}</td>
				</tr>
				<tr>
					<td colspan="1"><input type="Submit" name="Submit"
						Value="Update Information" /></td>
					<td colspan="1"><input type="Submit" name="Submit"
						Value="Get Profiles" /></td>
					<td><a href="${pageContext.request.contextPath}/customerRep">Cancel</a></td>
				</tr>
			</table>
		</form>

	</c:if>

	<!--        %%%%%%%%%%%%%%%%%%%%%%%%%%%PROFILE LIST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   -->
	<c:if test="${!empty customerProfileList}">


		<table border="1" cellpadding="5" cellspacing="1">
			<tr>
				<th>Profile ID</th>
				<th></th>



			</tr>
			<c:forEach items="${customerProfileList}" var="profile">
				<tr>
					<td>${profile.profileId}</td>
					<td>
						<form method="POST"
							action="${pageContext.request.contextPath}/customerRep">

							<button type="Submit" name="selectProfileButton"
								Value=${profile.profileId}>SELECT</button>
						</form>

					</td>

				</tr>
			</c:forEach>
		</table>

	</c:if>

	<!--        %%%%%%%%%%%%%%%%%%%%%%%%%UPDATE PROFILE FOR SPECIFIC %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   -->

	<c:if test="${!empty selectedProfile}">


		<form method="POST"
			action="${pageContext.request.contextPath}/customerRep">
			<input type="hidden" name="customerProfileID"
				value="${selectedProfile.profileId}" /> <input type="hidden"
				name="ownerSSN" value="${selectedProfile.ownerSSN}" />

			<table border="0">

				<tr>
					<td>Age</td>
					<td><input type="number" name="age"
						value="${selectedProfile.age}" required /></td>
				</tr>
				<tr>
					<td>Dating Age Range Start</td>
					<td><input type="number" name="datingAgeRangeStart"
						value="${selectedProfile.datingAgeRangeStart}" required /></td>
				</tr>
				<tr>
					<td>datingageRangeEnd</td>
					<td><input type="number" name="datingAgeRangeEnd"
						value="${selectedProfile.datingAgeRangeEnd}" required /></td>
				</tr>
				<tr>
					<td>Dating Geo Range</td>
					<td><input type="number" name="geoRange"
						value="${selectedProfile.datingGeoRange}" required /></td>
				</tr>
				<tr>
					<td>Hobbies</td>
					<td><input type="text" name="hobbies"
						value="${selectedProfile.hobbies}" required /></td>
				</tr>
				<tr>
					<td>Height</td>
					<td><input type="number" step="0.01" name="height"
						value="${selectedProfile.height}" required /></td>
				</tr>

				<tr>
					<td>Weight</td>
					<td><input type="number" name="weight"
						value="${selectedProfile.weight}" required /></td>
				</tr>
				<tr>
					<td>Color</td>
					<td><input type="text" name="color"
						value="${selectedProfile.color}" required /></td>
				</tr>
				<tr>
					<td>Creation Date</td>
					<input type="hidden" name="creationDate"
						value="${selectedProfile.creationDate}" />
					<td name="creationDateString" type"date" style="color: red;">${selectedProfile.creationDate}</td>
				</tr>
				<tr>
					<td>Last Modified</td>
					<td name="lastModDate" type"date" style="color: red;">${selectedProfile.lastModDate}</td>
				</tr>
				<tr>
					<td colspan="1"><input type="Submit" name="Submit"
						Value="Update Profile Information" /></td>
						<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/customerRep">

					<input type="Submit" name="Submit" Value="Create Date" />
				</form>

			</th>
					<td><a href="${pageContext.request.contextPath}/customerRep">Cancel</a></td>
				</tr>
			</table>
		</form>

	</c:if>









	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>