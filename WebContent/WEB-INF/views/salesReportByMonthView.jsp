<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Report List</title>
</head>
<body>

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_managerMenu.jsp"></jsp:include>

	<h3>Sales List</h3>


	<p style="color: red;">${errorString}</p>

	<table border="1" cellpadding="5" cellspacing="1">
		<!-- <tr>
			<th>Date_Time</th>
			<th>BookingFee</th>


		</tr>
		<c:forEach items="${salesReportByMonthList}" var="date">
			<tr>
				<td>${date.date_time}</td>
				<td>${date.bookingFee}</td>

			</tr>
		</c:forEach>

	</table> -->
		<tr>
			<th>Get Report By Month & Year</th>
			<th>Get Report By Specific Date</th>
			<th>Get Report For Specific Customer (By Profile Name)</th>
		</tr>
		<tr>
			<th><form method="POST"
					action="${pageContext.request.contextPath}/man/SalesReportByMonth">
					<select name="month">
						<option value="1">January</option>
						<option value="2">February</option>
						<option value="3">March</option>
						<option value="4">April</option>
						<option value="5">May</option>
						<option value="6">June</option>
						<option value="7">July</option>
						<option value="8">August</option>
						<option value="9">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select> <select name="year">
						<option value="2014">2014</option>
						<option value="2015">2015</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>
						<option value="2018">2018</option>
					</select> <input type="submit" Name="Submit" Value="GetReport" />
				</form></th>

			<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/man/SalesReportByMonth">

					<input type="date" name="dateValue" /> <input type="submit"
						name="Submit" Value="GetDate" />
				</form>
			</th>
			<th>
				<form method="POST"
					action="${pageContext.request.contextPath}/man/SalesReportByMonth">

					<input type="text" name="ssn" placeholder="XXX-XX-XXXX"
						pattern="\d{3}-?\d{2}-?\d{4}" required><input type="Submit"
						name="Submit" Value="Get Person Date" />
				</form>

			</th>



		</tr>

	</table>


	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>