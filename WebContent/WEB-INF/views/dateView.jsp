<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Make a Date</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h3>Make a Date</h3>

	<p style="color: red;">${errorString}</p>

	<c:if test="${not empty profiles}">
		<form method="POST"
			action="${pageContext.request.contextPath}/requestDate">
			<input type="hidden" name="profile1" value="${profileId}" />
			<table border="0">
				<tr>
					<td>Going on a Date with:</td>
					<td style="color: red;">${profileId}</td>
				</tr>
				<tr>
					<td>Choose one of your profiles: </td>
					<c:forEach items="${profiles}" var="pf">
					<td><input type="radio" name="profile2" value="${pf.profileId}">${pf.profileId}</td>
					</c:forEach>
				</tr>
				<tr>
					<td>Date/Time: </td>
					<td><input type="datetime-local" name="date"></td>			
				</tr>
				<tr>
					<td>Location: </td>
					<td><input type="text" name="location"></td>			
				</tr>
				<tr>
					<td>Comments: </td>
					<td><input type="text" name="comments"></td>			
				</tr>
				<tr>
					<td>Booking Fee: </td>
					<td>$50</td>			
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /> <a
						href="${pageContext.request.contextPath}/Account">Cancel</a></td>
				</tr>
			</table>
		</form>
	</c:if>

	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>