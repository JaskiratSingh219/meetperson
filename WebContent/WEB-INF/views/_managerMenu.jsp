<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
    
<div>
 
   <a href="${pageContext.request.contextPath}/man/employeeList">Employee List</a>
   |
   <a href="${pageContext.request.contextPath}/man/userList">User List</a>
   |
   <a href="${pageContext.request.contextPath}/man/SalesReportByMonth">Sales View</a>
   |
   <a href="${pageContext.request.contextPath}/man/mostProfitableRep">Most Revenue By Customer Rep</a>
   |
   <a href="${pageContext.request.contextPath}/man/mostProfitable">Most Revenue By Customer</a>
   |
   <a href="${pageContext.request.contextPath}/man/mostActive">Most Active Customer</a>
   |
   <a href="${pageContext.request.contextPath}/man/highestRatedCustomer">Highest Rated Customer</a>
   |
   <a href="${pageContext.request.contextPath}/man/highestRatedDates">Highest Rated Dates</a>
   |
   <a href="${pageContext.request.contextPath}/man/backup">Backup</a>
    
</div>  