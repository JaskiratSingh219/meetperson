<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account View</title>
</head>
<body bgcolor="#B0F8FD">

	<jsp:include page="_header.jsp"></jsp:include>
	<jsp:include page="_menu.jsp"></jsp:include>

	<h2>&nbsp;Welcome, ${user.firstName}!!</h2>
	<ul>
		<li>Membership: ${user.ppp }</li>
		<li>Rating: ${user.rating }</li>
	</ul>
	<a href="browse?ssn=${user.ssn}">Browse</a>
	<a href="search?ssn=${user.ssn}">Search</a>
	<table style="width: 1061px; height: 150px;">
		<tbody>
			<tr>
				<td style="width: 280.833px;">
					<p>&nbsp;Upcoming Dates</p>
				</td>
				<td style="width: 465.167px;">
					<p>Past Dates</p>
				</td>
				<td style="width: 465.167px;">
					<p>Favorites</p>
				</td>
				<td style="width: 290px;">
					<p>Profiles</p>
				</td>
			</tr>
			<tr>
				<td style="width: 280.833px;"><c:forEach
						items="${upcomingdates}" var="ud">
						<c:url var="commentFuture" value="/comment">
							<c:param name="pd1" value="${ud.profile1}" />
							<c:param name="pd2" value="${ud.profile2}" />
							<c:param name="pdt" value="${ud.date_time}" />
							<c:param name="pdc" value="${ud.comments}" />
						</c:url>
						<c:url var="cancelDate" value="/cancelDate">
							<c:param name="pd1" value="${ud.profile1}" />
							<c:param name="pd2" value="${ud.profile2}" />
							<c:param name="pdt" value="${ud.date_time}" />
						</c:url>
						<ul>
							<li>Date between ${ud.profile1} and ${ud.profile2} at
								${ud.date_time}
								<p>
									<a href="${commentFuture}">View/Add Comments</a> <a
										href="${cancelDate}">Cancel Date</a>
								</p>
							</li>
						</ul>
					</c:forEach> <c:if test="${empty upcomingdates}">
						<p>There are no upcoming dates.</p>
					</c:if></td>
				<td style="width: 465.167px;"><c:forEach items="${pastDates}"
						var="pd">
						<c:url var="pastFuture" value="/comment">
							<c:param name="pd1" value="${pd.profile1}" />
							<c:param name="pd2" value="${pd.profile2}" />
							<c:param name="pdt" value="${pd.date_time}" />
							<c:param name="pdc" value="${pd.comments}" />
						</c:url>
						<ul>
							<li>Date between ${pd.profile1} and ${pd.profile2} at
								${pd.date_time}
								<p>
									<a href="${pastFuture}">View/Add Comments</a>
								</p>
							</li>
						</ul>
					</c:forEach> <c:if test="${empty pastDates}">
						<p>There are no past dates.</p>
					</c:if></td>
				<td style="width: 465.167px;"><c:forEach items="${favorites}"
						var="fd">
						<ul>
							<li>${fd.likee}</li>
						</ul>
					</c:forEach> <c:if test="${empty favorites}">
						<p>There are no favorites.</p>
					</c:if></td>
				<td style="width: 290px;"><c:forEach items="${profiles}"
						var="pf">
						<ul>
							<li>${pf.profileId}</li>
						</ul>
					</c:forEach> <c:if test="${empty pastDates}">
						<p>There are no profiles.</p>
					</c:if></td>
			</tr>
		</tbody>
	</table>

	<h3>&nbsp;Recommended</h3>
	<table style="width: 1059px;">
		<tbody>
			<tr style="height: 82.1333px;">
				<c:forEach items="${recs}" var="rc">
					<td >${rc}</td>
				</c:forEach>
			</tr>
		</tbody>
	</table>
	<p>&nbsp;</p>

	<p style="color: red;">${errorString}</p>

	<jsp:include page="_footer.jsp"></jsp:include>

</body>
</html>